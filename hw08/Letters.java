//Grant Hershman
//CSE2
//April 6, 2019
//Program uses an array to generate a random character array and two more Arrays
//ranging from A to M and N to Z.
import java.util.Random;
import java.util.Arrays;
public class Letters{//start of class
  public static void main(String[] args){//start of main method
    Random randomGenerator = new Random();//initializes random function
    int randomInt = randomGenerator.nextInt(18);//random int from 0 to 18
    while (randomInt<4){//while loop allows array of characters to be greater than 4
      randomInt = randomGenerator.nextInt(18);
    }
    System.out.print("Random character array: ");
    char[] y = getAtoM(randomInt);//calls function for characters from a to m
    char[] g = getNtoZ(randomInt);//calls function for characters from n to z
    System.out.println();
    System.out.print("AtoM characters: ");
    char[] x = getAtoM(randomInt);
    System.out.println();
    System.out.print("NtoZ characters: ");
    char[] z = getNtoZ(randomInt);
    System.out.println();
  }
  public static char[] getAtoM(int randomInt){//method for a to m
    int y = randomInt/2;//first half of array
    char output = 'a';//initializes output
    char[] numArrays;
    numArrays = new char[y];
    for (int i = 0; i<=(y-1); i++){
      Random randomGenerator = new Random();
      int randomNum = randomGenerator.nextInt(25);
      switch (randomNum){//picks letter based on random number
        case 0:
          output = 'A';
          break;
        case 1:
          output = 'a';
          break;
        case 2:
          output = 'B';
          break;
        case 3:
          output = 'b';
          break;
        case 4:
          output = 'C';
          break;
        case 5:
          output = 'c';
          break;
          case 6:
            output = 'D';
            break;
          case 7:
            output = 'd';
            break;
          case 8:
            output = 'E';
            break;
          case 9:
            output = 'e';
            break;
          case 10:
            output = 'F';
            break;
          case 11:
            output = 'f';
            break;
            case 12:
              output = 'G';
              break;
            case 13:
              output = 'g';
              break;
            case 14:
              output = 'H';
              break;
            case 15:
              output = 'h';
              break;
            case 16:
              output = 'I';
              break;
            case 17:
              output = 'i';
              break;
              case 18:
                output = 'J';
                break;
              case 19:
                output = 'j';
                break;
              case 20:
                output = 'K';
                break;
              case 21:
                output = 'k';
                break;
              case 22:
                output = 'L';
                break;
              case 23:
                output = 'l';
                break;
                case 24:
                  output = 'M';
                  break;
                case 25:
                  output = 'm';
                  break;
    }
    numArrays[i] = output;
    System.out.print(output);
    }
    return numArrays;
  }
  public static char[] getNtoZ(int randomInt){//method for n to z
    int y = randomInt/2;
    char output = 'a';
    char[] numArrays;
    numArrays = new char[y];
    for (int i = 0; i<=(y-1); i++){
      Random randomGenerator = new Random();
      int randomNum = randomGenerator.nextInt(25);
      switch (randomNum){//picks random letter
        case 0:
          output = 'N';
          break;
        case 1:
          output = 'n';
          break;
        case 2:
          output = 'O';
          break;
        case 3:
          output = 'o';
          break;
        case 4:
          output = 'P';
          break;
        case 5:
          output = 'p';
          break;
          case 6:
            output = 'Q';
            break;
          case 7:
            output = 'q';
            break;
          case 8:
            output = 'R';
            break;
          case 9:
            output = 'r';
            break;
          case 10:
            output = 'S';
            break;
          case 11:
            output = 's';
            break;
            case 12:
              output = 'T';
              break;
            case 13:
              output = 't';
              break;
            case 14:
              output = 'U';
              break;
            case 15:
              output = 'u';
              break;
            case 16:
              output = 'V';
              break;
            case 17:
              output = 'v';
              break;
              case 18:
                output = 'W';
                break;
              case 19:
                output = 'w';
                break;
              case 20:
                output = 'X';
                break;
              case 21:
                output = 'x';
                break;
              case 22:
                output = 'Y';
                break;
              case 23:
                output = 'y';
                break;
                case 24:
                  output = 'Z';
                  break;
                case 25:
                  output = 'z';
                  break;
    }
    numArrays[i] = output;
    System.out.print(output);
    }
    return numArrays;
  }
}//end of class
