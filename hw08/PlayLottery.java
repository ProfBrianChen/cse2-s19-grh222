//Grant Hershman
//CSE2
//April 6, 2019
//This Program allows the input of six integars and compares them to six other ints to see if you won the Lottery.
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class PlayLottery{//start of class
  public static void main(String[] args){//start of main method
    int[] numArrays;//initializes Arrays
    numArrays = new int[6];//sets array limit to 6
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter 5 numbers between 0 and 59: ");
    int i;
    for (i = 0; i <= 4; i++){
      int input = scan.nextInt();
      numArrays[i] = input;
    }
    System.out.print("Your numbers are: ");
    for (int j = 0; j<=4; j++){//prints out your inputted values
    System.out.print(numArrays[j] + " ");
    }
    System.out.println();
    int[] actual = numbersPicked();
    boolean x = userWins(numArrays, actual);//calls method to see if you won or not
    if (x == true){
      System.out.println("You're rich!");
    }
    if (x == false){
      System.out.println("You lose");
    }
  }
  public static boolean userWins(int[] numArrays, int[] actual){//userWins method
    boolean q = true;//initializes variable true
    for (int i = 0; i <= 4; i++){
      if (numArrays[i] == actual[i]){//compares values
      q = true;
    }
    else{
      q = false;
    }
  }
    return q;
  }
  public static int[] numbersPicked(){//method to pick 5 random numbers
    Random randomGenerator = new Random();
    int[] numArrays2;
    numArrays2 = new int[6];
    int i;
    System.out.print("The winning numbers are: ");
    for (i = 0; i <= 4; i++){
      int randomNum = randomGenerator.nextInt(59);
      numArrays2[i] = randomNum;
      System.out.print(numArrays2[i] + " ");
    }
    System.out.println();
    return numArrays2;
  }
}//end of class
