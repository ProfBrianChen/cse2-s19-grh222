//Grant Hershman
//March 10, 2019
//CSE2
//Program will print a visible network that adapts to inputs from the user. Each box will have a size as well as the total size of the 
//printed network. I was unable to get the network to print to repeat horizontally even after meeting with two TA's. 
//However, I do have the vertical part working well. 
import java.util.Scanner;
public class Network{
  public static void main (String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Input height of box:");
    int heightNetwork;
    int connector;
    while (!scan.hasNextInt()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type positive integar: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    heightNetwork = scan.nextInt(); //takes input from user 
    
    System.out.print("Input width of box:");
    int widthNetwork;
    while (!scan.hasNextInt()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type positive integar: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    widthNetwork = scan.nextInt(); //takes input from user
    
    System.out.print("Input total height of network:");
    int heightTotal;
    while (!scan.hasNextInt()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type positive integar: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    heightTotal = scan.nextInt(); //takes input from user 
    
    System.out.print("Input total width of network:");
    int widthTotal;
    while (!scan.hasNextInt()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type positive integar: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    widthTotal = scan.nextInt(); //takes input from user 
    
    int check = heightTotal;//check will be as a sort of counter. when check runs out, program will end
    int totalRows = heightTotal;//totalRows is used to set up parameters for main loop
    
  
    
    for (int numBoxes = 1; (numBoxes <= (totalRows - heightNetwork)) || (totalRows == heightNetwork) || (totalRows<heightNetwork); numBoxes++){
    //line above sets limit for for loop
    System.out.print("#"); //start of top of box
    for (int columns = 1; columns <= (widthNetwork-2); columns++){//for loop for the width
      System.out.print("-");//dashes
    }//end of for loop for width on top
    System.out.print("#");//corner
      if (heightNetwork == 1){//if statement for if the input was only of size one
        for (int rightConnector = 1; rightConnector<=4; rightConnector++){//for loop for right connector on side
        System.out.print("-");//connector itself
        }//end of loop
      }//end of if statement
    System.out.println();
      check--;//counts the total number of lines used
     
    
    for (int rows = 1; rows <= (heightNetwork-2); rows++){//for loop for height of box
      System.out.print("|");
      for (int tabs = 1; tabs <= (widthNetwork-2); tabs++){//for loop for what is printed in each line of box
        System.out.print(" ");
      }
      System.out.print("|");
      if ((rows == ((heightNetwork/2)-1)) || (rows == (heightNetwork/2))){//if statement for conditions the connector is made
        for (int rightConnector = 1; rightConnector<=4; rightConnector++){//loop used to make right connector
        System.out.print("-");
        }
        
      }
      if (check <= 0){
        break;
      }//ends loop if total height is met
   
      System.out.println();
      
      
      check--;//decreases amount of lines available
      
      totalRows--;//count used to see if another box should be made
      
    }//end of height loop
      
      if (check == 0){
        break;
      }//ends loop if total height is met
   
    
    System.out.print("#");//start of bottom of box
    for (int columns = 1; columns <= (widthNetwork-2); columns++){//loop for bottom of box
      System.out.print("-");
    }
    System.out.print("#");
    System.out.println();
      check--;//decreases amount of lines available
      
      
      
      
      if (check >0){//if lines are still available after box is done, a bottom connector will be made
     for (int bottomConnector = 1; bottomConnector<= 4; bottomConnector++){//loop for height of connector
          for (int bottomTabs = 1; bottomTabs<= ((widthNetwork/2)-1); bottomTabs++){//loop for each line of connector
          System.out.print(" ");
          }
          System.out.println("||");//conector itself
          check--;//decreases amount of lines available
          if ((check == 0) || (check<0)){
            break;
          }//ends loop if total height is met
       
      }
      }
        if (check <= 0){
          break;
        }//ends loop if total height is met
   
   
      
    }
System.out.println();
    }
  }
