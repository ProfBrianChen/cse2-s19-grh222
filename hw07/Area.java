//Grant Hershman
//CSE2
//March 24, 2018
//cdkjsbjkdbv

import java.util.Scanner;
public class Area{//start of class
  public static void main(String[] args){//start of main method
    Scanner scan = new Scanner(System.in);
    System.out.println("What shape do you want to find the area of (rectangle, triangle, or circle)?");
    String shape = input();//input for name of shape
    String shape1 = "rectangle";//check for rectangle
    String shape2 = "triangle";//check for triangle
    String shape3 = "circle";//check for circle
    if (shape.equals(shape1)){//runs code for rectangle
      System.out.println("The area of the rectangle is " + Rectangle());
    }
    if (shape.equals(shape2)){//runs code for triangle
      System.out.println("The area of the triangle is " + Triangle());
    }
    if (shape.equals(shape3)){//runs code for circle
      System.out.println("The area of the circle is " + Circle());
    }
    while (!shape.equals(shape1) && !shape.equals(shape2) && !shape.equals(shape3)){//if no imput is correct
      System.out.println("You entered an invalid shape.");
      System.out.println("What shape do you want to find the area of (rectangle, triangle, or circle)?");
      String shape4 = input();//takes new input
      String shape5 = "rectangle";//checks for rectangle
      String shape6 = "triangle";//checks for triangle
      String shape7 = "circle";//checks for circle
      if (shape4.equals(shape5)){
        System.out.println("The area of the rectangle is " + Rectangle());
      }
      if (shape4.equals(shape6)){
        System.out.println("The area of the triangle is " + Triangle());
      }
      if (shape4.equals(shape7)){
        System.out.println("The area of the circle is " + Circle());
      }
      if (shape4.equals(shape5) || shape4.equals(shape6) || shape4.equals(shape7)){
        break;
      }
    }
  }

  public static double Rectangle(){//code for rectangle
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter length of rectangle: ");
    double length;
    while (!scan.hasNextDouble()){ //checks whether or not input is an integaar
      System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
      String junkWord = scan.next(); //resests scanner for new input
    } //end of while loop to test for integar input
    length = scan.nextDouble(); //input for length
    System.out.print("Enter width of rectangle: ");
    double width;
    while (!scan.hasNextDouble()){ //checks whether or not input is an integaar
      System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
      String junkWord = scan.next(); //resests scanner for new input
    } //end of while loop to test for integar input
    width = scan.nextDouble(); //input for length
    double area = length * width;
    return area;
  }

  public static double Triangle(){//method for triangle
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter base of triangle: ");
    double base;
    while (!scan.hasNextDouble()){ //checks whether or not input is an integaar
      System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
      String junkWord = scan.next(); //resests scanner for new input
    } //end of while loop to test for integar input
    base = scan.nextDouble(); //input for length
    System.out.print("Enter height of triangle: ");
    double height;
    while (!scan.hasNextDouble()){ //checks whether or not input is an integaar
      System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
      String junkWord = scan.next(); //resests scanner for new input
    } //end of while loop to test for integar input
    height = scan.nextDouble(); //input for length
    double area = .5 * (base * height);
    return area;
  }

  public static double Circle(){//method for circle
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter radius of circle: ");
    double radius;
    while (!scan.hasNextDouble()){ //checks whether or not input is an integaar
      System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
      String junkWord = scan.next(); //resests scanner for new input
    } //end of while loop to test for integar input
    radius = scan.nextDouble(); //input for length
    double area = 3.14 * radius * radius;
    return area;
  }

  public static String input(){//method for input of shape
    Scanner scan = new Scanner(System.in);
    String shape = scan.next();
    return shape;//returns string
  }
}
