//Grant Hershman
//CSE2
//March 25, 2019
//Program will ask for a random word. The user decides if they want the whole word analyzed or a certain amount of numbers.
//The one method will analyze if each position of a letter in the word is a character.
//The other method analzes a certain amount of letters to see if they are characters.
import java.util.Scanner;//scanner
public class StringAnalysis{//class
  public static void main(String[] args){//main method
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter a random word: ");
    String word = scan.next();//input for word
    System.out.print("Would you like to examine the whole word (yes/no): ");
    String answer = scan.next();//input for yes or no
    String answer1 = "yes";//string to compare to yes
    String answer2 = "no";//string to compare to yes
    int length = word.length();//checks length of word
    if (answer.equals(answer1)){//code for yes
      for (int check = 1; check <= length; check++){//parameters for word
      System.out.println(Input(word, check));
    }
    }
    if (answer.equals(answer2)){//code for no
      System.out.print("How many letters do you want to check? ");
      int num;
      while (!scan.hasNextInt()){ //checks whether or not input is an integar
        System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
        String junkWord = scan.next(); //resests scanner for new input
      } //end of while loop to test for integar input
      num = scan.nextInt(); //input for length
      if (num>length){
        System.out.println("Number is greater then length of word. Run program again.");
        System.exit(0);
      }
      for (int check = 1; check <= num; check++){
      System.out.println(Input(word, check));
    }
    }
  }

  public static boolean Input(String word, int check){//Input method
    char letter = word.charAt(check-1);//evaluates character at certain position
    boolean name = Character.isLetter(letter);//checks if letter is a char
    return name;//returns boolean name
  }
}
