//Grant Hershman
//CSE2
//February 8, 2019
//This program takes three imputs: length, width, and height and calculates the volume of the resulting box.
import java.util.Scanner; //imports java utility to make inputs
public class BoxVolume { //start of class
  public static void main(String[] args){ //start of main method
    Scanner myScanner = new Scanner (System.in); //sets up scanner to make inputs
    System.out.print("The width of the box is: "); //prints out prompt for input of width
    double width = myScanner.nextDouble(); //input for width value
    System.out.print("The length of the box is: "); //prints out prompt for input of length
    double length = myScanner.nextDouble(); //input for length value
    System.out.print("The height of the box is: "); //prints out prompt for input of height
    double height = myScanner.nextDouble(); //input for height value
    double volume = width * length * height; //calculation for volume of box
    System.out.println("The volume of the box is: " + volume); //prints out volume of box
  } //end of main method
} //end of class