//Grant Hershman
//CSE2
//February 8,2019
//This program converts an inputted value in meters to inches and prints out the result
import java.util.Scanner; //imports java utility to make inputs
public class Convert { //start of class
  public static void main(String[] args){ //start of main method
    Scanner myScanner = new Scanner (System.in); //sets up scanner to make inputs
    System.out.print("Enter the distance in meters: "); //prints out prompt for user to input distance value
    double distanceMeters = myScanner.nextDouble(); //creates input for distance value in meters
    double distanceInches = distanceMeters * 39.37; //converts inputted value to inches
    System.out.println(distanceMeters + " meters is " + distanceInches + " inches."); //prints out inputted value and converted value
  } //end of main method
} //end of class