//Grant Hershman
//CSE2
//April 29,2019
//This program generates a matrix of random height and width and assigns a population to each cell.
//Then it has a negative number appear on some blocks.
//The final method moved the alians over one block.
import java.util.Arrays;
import java.util.Random;
public class RobotCity{//start of class
  public static void main(String[] args){//main method
    for(int i = 0; i<5; i++){//5 different buildings and attacks
      System.out.println("City " + (i+1));
      buildCity();
    }
    System.out.println();
    System.out.println();
  }
  public static void buildCity(){//creates building
    Random randomGenerator = new Random();
    int height = randomGenerator.nextInt(15);//random height
    while (height < 10){
      height = randomGenerator.nextInt(15);
    }
    int width = randomGenerator.nextInt(15);//random width
    while (width < 10){
      width = randomGenerator.nextInt(15);
    }
    int[][] cityArray = new int[height][];
    for (int i = 0; i<=(height-1); i++){
      cityArray[i] = new int[width];
      for (int j = 0; j<=(width-1); j++){
        int num = randomGenerator.nextInt(999);
        while (num < 100){
          num = randomGenerator.nextInt(999);//random number for population
        }
        cityArray[i][j] = num;//assigns random number to array
      }
    }
    display(cityArray, height, width);
    int k = randomGenerator.nextInt(30);
    int[][] x = invade(cityArray, k, height, width);
    display(x, height, width);
    update(x, height, width);
    display(x, height, width);
  }
  public static void display(int[][] cityArray, int height, int width){//print method
    for (int i = 0; i<=(height-1); i++){
      for (int j = 0; j<=(width-1); j++){
        System.out.print(cityArray[i][j] + " ");
      }
      System.out.println();
    }
  }
  public static int[][] invade(int[][] cityArray, int k, int x, int y){//gets aliens to invade
    System.out.println();
    System.out.println();
    Random randomGenerator = new Random();
    for (int i = 0; i < k; i++){
      int height = randomGenerator.nextInt(15);
      while (height >= x){
        height = randomGenerator.nextInt(15);
      }
      int width = randomGenerator.nextInt(15);
      while (width >= y){
        width = randomGenerator.nextInt(15);
      }
      cityArray[height][width] = cityArray[height][width] * -1;//makes certain blocks negative
    }
    return cityArray;
  }
  public static void update(int[][] cityArray, int height, int width){//method that moves aliens over one block to the right
    System.out.println();
    System.out.println();
    for (int i = 0; i<=(height-1); i++){
      for (int j = 0; j<=(width-1); j++){
          if (cityArray[i][j] < 0){//ensures if alien is on right-most line, the alien leaves
            cityArray[i][j] = cityArray[i][j] * -1;
            if ((j+1) < width){
              cityArray[i][j+1] = cityArray[i][j+1] * -1;
              j = j + 2;
            }
          }
      }
    }
  }
}
