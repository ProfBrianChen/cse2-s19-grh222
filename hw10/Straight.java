//Grant Hershman
//CSE2
//April 29,2019
//This program generates 1000000 different hands of cards and checks the frequency of straights occurring.
import java.util.Arrays;
import java.util.Random;
public class Straight{//start of class
  public static void main(String[] args){//start of main method
    int q = 0;//counter
    for (int i = 0; i<1000000; i++){//1000000 hands
      int x = shuffle();
      if (x==1){
        q++;
      }
    }
    double y = q;//converts q to a double
    double avg = (y / 10000.0);//calculates average
    System.out.println(avg + "%");
  }
  public static int shuffle(){//shuffle method
    int[] deck = new int[52];
    Random randomGenerator = new Random();
    for (int i = 0; i < deck.length; i++){
      deck[i] = randomGenerator.nextInt(52);
    }
    int x = draw(deck);
    return x;
  }
  public static int draw(int[] deck){//draw method
    int[] drawn = new int[5];
    for (int i = 0; i<5; i++){
      drawn[i] = deck[i];
    }
    Random randomGenerator = new Random();
    int k = randomGenerator.nextInt(5);
    int x = search(drawn, k);
    return x;
  }
  public static int search(int[] drawn, int k){//search method
    for (int i = 0; i<5; i++){//converts cards to value from 1-13
      if ((drawn[i] >= 13) && (drawn[i] <= 25)){
        drawn[i] = drawn[i] - 13;
      }
      else if ((drawn[i] >= 26) && (drawn[i] <= 38)){
        drawn[i] = drawn[i] - 26;
      }
      else if ((drawn[i] >= 39) && (drawn[i] <= 52)){
        drawn[i] = drawn[i] - 39;
      }
    }
    Arrays.sort(drawn);
    int x = straight(drawn);
    return x;
  }
  public static int straight(int[] drawn){//method checks for straight
    int x = 0;
      if ((drawn[4] == (drawn[3] + 1)) && (drawn[3] == (drawn[2] + 1)) && (drawn[2] == (drawn[1] + 1)) && (drawn[1] == (drawn[0] + 1))){
        x++;
      }
      return x;
  }
}
