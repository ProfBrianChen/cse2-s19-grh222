//Grant Hershman
//CSE2
//February 15, 2019
//This program simulates picking four random cards from seperate decks.
//Then the program tells the user whether they have a pair, two pair, three of a kind, or high card hand
//import java.util.Scanner;
import java.util.Random; //imports ability to use a random number generator
public class PokerHandCheck { //start of class
  public static void main(String[] args){ //start of main method
    //Scanner myScanner = new Scanner (System.in); 
    System.out.println("Your cards are:"); //first print line the user reads
    //int card1 = myScanner.nextInt();
    int card1 = (int)(Math.random() * 52 + 1); //sets the variable to select a random number from 1 to 52
    if (card1 == 1){ //start of if statement for ace of diamonds
      System.out.println("Ace of diamonds"); //prints out the card selected
    } //end of if statement for ace of diamonds
    else if (card1 == 2){ //start of if statement for 2 of diamonds
      System.out.println("2 of diamonds"); //prints out the card selected
    } //end of if statement for 2 of diamonds
    else if (card1 == 3){ //start of if statement for 3 of diamonds
      System.out.println("3 of diamonds"); //prints out the card selected
    } //end of if statement for 3 of diamonds
    else if (card1 == 4){ //start of if statement for 4 of diamonds
      System.out.println("4 of diamonds"); //prints out the card selected
    } //end of if statement for 4 of diamonds
    else if (card1 == 5){ //start of if statement for 5 of diamonds
      System.out.println("5 of diamonds"); //prints out the card selected
    } //end of if statement for 5 of diamonds
    else if (card1 == 6){ //start of if statement for 6 of diamonds
      System.out.println("6 of diamonds"); //prints out the card selected
    } //end of if statement for 6 of diamonds
    else if (card1 == 7){ //start of if statement for 7 of diamonds
      System.out.println("7 of diamonds"); //prints out the card selected
    } //end of if statement for 7 of diamonds
    else if (card1 == 8){ //start of if statement for 8 of diamonds
      System.out.println("8 of diamonds"); //prints out the card selected
    } //end of if statement for 8 of diamonds
    else if (card1 == 9){ //start of if statement for 9 of diamonds
      System.out.println("9 of diamonds"); //prints out the card selected
    } //end of if statement for 9 of diamonds
    else if (card1 == 10){ //start of if statement for 10 of diamonds
      System.out.println("10 of diamonds"); //prints out the card selected
    } //end of if statement for 10 of diamonds
    else if (card1 == 11){ //start of if statement for jack  of diamonds
      System.out.println("Jack of diamonds"); //prints out the card selected
    } //end of if statement for jack of diamonds
    else if (card1 == 12){ //start of if statement for queen of diamonds
      System.out.println("Queen of diamonds"); //prints out the card selected
    } //end of if statement for queen of diamonds
    else if (card1 == 13){ //start of if statement for king of diamonds
      System.out.println("King of diamonds"); //prints out the card selected
    } //end of if statement for king of diamonds
    else if (card1 == 14){ //start of if statement for ace of clubs
      System.out.println("Ace of clubs"); //prints out the card selected
    } //end of if statement for ace of clubs
    else if (card1 == 15){ //start of if statement for 2 of clubs
      System.out.println("2 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card1 == 16){ //start of if statement for 3 of clubs
      System.out.println("3 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card1 == 17){ //start of if statement for 4 of clubs
      System.out.println("4 of clubs"); //prints out the card selected
    } //end of if statement for 4 of clubs
    else if (card1 == 18){ //start of if statement for 5 of clubs
      System.out.println("5 of clubs"); //prints out the card selected
    } //end of if statement for 5 of clubs
    else if (card1 == 19){ //start of if statement for 6 of clubs
      System.out.println("6 of clubs"); //prints out the card selected
    } //end of if statement for 6 of clubs
    else if (card1 == 20){ //start of if statement for 7 of clubs
      System.out.println("7 of clubs"); //prints out the card selected
    } //end of if statement for 7 of clubs
    else if (card1 == 21){ //start of if statement for 8 of clubs
      System.out.println("8 of clubs"); //prints out the card selected
    } //end of if statement for 8 of clubs
    else if (card1 == 22){ //start of if statement for 9 of clubs
      System.out.println("9 of clubs"); //prints out the card selected
    } //end of if statement for 9 of clubs
    else if (card1 == 23){ //start of if statement for 10 of clubs
      System.out.println("10 of clubs"); //prints out the card selected
    } //end of if statement for 10 of clubs
    else if (card1 == 24){ //start of if statement for jack of clubs
      System.out.println("Jack of clubs"); //prints out the card selected
    } //end of if statement for jack of clubs
    else if (card1 == 25){ //start of if statement for queen of clubs
      System.out.println("Queen of clubs"); //prints out the card selected
    } //end of if statement for queen of clubs
    else if (card1 == 26){ //start of if statement for king of clubs
      System.out.println("King of clubs"); //prints out the card selected
    } //end of if statement for king of clubs
    else if (card1 == 27){ //start of if statement for ace of hearts
      System.out.println("Ace of hearts"); //prints out the card selected
    } //end of if statement for ace of hearts
    else if (card1 == 28){ //start of if statement for 2 of hearts
      System.out.println("2 of hearts"); //prints out the card selected
    } //end of if statement for 2 of hearts
    else if (card1 == 29){ //start of if statement for 3 of hearts
      System.out.println("3 of hearts"); //prints out the card selected
    } //end of if statement for 3 of hearts
    else if (card1 == 30){ //start of if statement for 4 of hearts
      System.out.println("4 of hearts"); //prints out the card selected
    } //end of if statement for 4 of hearts
    else if (card1 == 31){ //start of if statement for 5 of hearts
      System.out.println("5 of hearts"); //prints out the card selected
    } //end of if statement for 5 of hearts
    else if (card1 == 32){ //start of if statement for 6 of hearts
      System.out.println("6 of hearts"); //prints out the card selected
    } //end of if statement for 6 of hearts
    else if (card1 == 33){ //start of if statement for 7 of hearts
      System.out.println("7 of hearts"); //prints out the card selected
    } //end of if statement for 7 of hearts
    else if (card1 == 34){ //start of if statement for 8 of hearts
      System.out.println("8 of hearts"); //prints out the card selected
    } //end of if statement for 8 of hearts
    else if (card1 == 35){ //start of if statement for 9 of hearts
      System.out.println("9 of hearts"); //prints out the card selected
    } //end of if statement for 9 of hearts
    else if (card1 == 36){ //start of if statement for 10 of hearts
      System.out.println("10 of hearts"); //prints out the card selected
    } //end of if statement for 10 of hearts
    else if (card1 == 37){ //start of if statement for jack of hearts
      System.out.println("Jack of hearts"); //prints out the card selected
    } //end of if statement for jack of hearts
    else if (card1 == 38){ //start of if statement for queen of hearts
      System.out.println("Queen of hearts"); //prints out the card selected
    } //end of if statement for queen of hearts
    else if (card1 == 39){ //start of if statement for king of hearts
      System.out.println("King of hearts"); //prints out the card selected
    } //end of if statement for king of hearts
    else if (card1 == 40){ //start of if statement for ace of spades
      System.out.println("Ace of spades"); //prints out the card selected
    } //end of if statement for ace of spades
    else if (card1 == 41){ //start of if statement for 2 of spades
      System.out.println("2 of spades"); //prints out the card selected
    } //end of if statement for 2 of spades
    else if (card1 == 42){ //start of if statement for 3 of spades
      System.out.println("3 of spades"); //prints out the card selected
    } //end of if statement for 3 of spades
    else if (card1 == 43){ //start of if statement for 4 of spades
      System.out.println("4 of spades"); //prints out the card selected
    } //end of if statement for 4 of spades
    else if (card1 == 44){ //start of if statement for 5 of spades
      System.out.println("5 of spades"); //prints out the card selected
    } //end of if statement for ace 5 spades
    else if (card1 == 45){ //start of if statement for 6 of spades
      System.out.println("6 of spades"); //prints out the card selected
    } //end of if statement for 6 of spades
    else if (card1 == 46){ //start of if statement for 7 of spades
      System.out.println("7 of spades"); //prints out the card selected
    } //end of if statement for 7 of spades
    else if (card1 == 47){ //start of if statement for 8 of spades
      System.out.println("8 of spades"); //prints out the card selected
    } //end of if statement for 8 of spades
    else if (card1 == 48){ //start of if statement for 9 of spades
      System.out.println("9 of spades"); //prints out the card selected
    } //end of if statement for 9 of spades
    else if (card1 == 49){ //start of if statement for 10 of spades
      System.out.println("10 of spades"); //prints out the card selected
    } //end of if statement for 10 of spades
    else if (card1 == 50){ //start of if statement for jack of spades
      System.out.println("Jack of spades"); //prints out the card selected
    } //end of if statement for jack of spades
    else if (card1 == 51){ //start of if statement for queen of spades
      System.out.println("Queen of spades"); //prints out the card selected
    } //end of if statement for queen of spades
    else if (card1 == 52){ //start of if statement for king of spades
      System.out.println("King of spades"); //prints out the card selected
    } //end of if statement for king of spades
    //int card2 = myScanner.nextInt();
    int card2 = (int)(Math.random() * 52 + 1); //sets the variable to select a random number from 1 to 52
    if (card2 == 1){ //start of if statement for ace of diamonds
      System.out.println("Ace of diamonds"); //prints out the card selected
    } //end of if statement for ace of diamonds
    else if (card2 == 2){ //start of if statement for 2 of diamonds
      System.out.println("2 of diamonds"); //prints out the card selected
    } //end of if statement for 2 of diamonds
    else if (card2 == 3){ //start of if statement for 3 of diamonds
      System.out.println("3 of diamonds"); //prints out the card selected
    } //end of if statement for 3 of diamonds
    else if (card2 == 4){ //start of if statement for 4 of diamonds
      System.out.println("4 of diamonds"); //prints out the card selected
    } //end of if statement for 4 of diamonds
    else if (card2 == 5){ //start of if statement for 5 of diamonds
      System.out.println("5 of diamonds"); //prints out the card selected
    } //end of if statement for 5 of diamonds
    else if (card2 == 6){ //start of if statement for 6 of diamonds
      System.out.println("6 of diamonds"); //prints out the card selected
    } //end of if statement for 6 of diamonds
    else if (card2 == 7){ //start of if statement for 7 of diamonds
      System.out.println("7 of diamonds"); //prints out the card selected
    } //end of if statement for 7 of diamonds
    else if (card2 == 8){ //start of if statement for 8 of diamonds
      System.out.println("8 of diamonds"); //prints out the card selected
    } //end of if statement for 8 of diamonds
    else if (card2 == 9){ //start of if statement for 9 of diamonds
      System.out.println("9 of diamonds"); //prints out the card selected
    } //end of if statement for 9 of diamonds
    else if (card2 == 10){ //start of if statement for 10 of diamonds
      System.out.println("10 of diamonds"); //prints out the card selected
    } //end of if statement for 10 of diamonds
    else if (card2 == 11){ //start of if statement for jack  of diamonds
      System.out.println("Jack of diamonds"); //prints out the card selected
    } //end of if statement for jack of diamonds
    else if (card2 == 12){ //start of if statement for queen of diamonds
      System.out.println("Queen of diamonds"); //prints out the card selected
    } //end of if statement for queen of diamonds
    else if (card2 == 13){ //start of if statement for king of diamonds
      System.out.println("King of diamonds"); //prints out the card selected
    } //end of if statement for king of diamonds
    else if (card2 == 14){ //start of if statement for ace of clubs
      System.out.println("Ace of clubs"); //prints out the card selected
    } //end of if statement for ace of clubs
    else if (card2 == 15){ //start of if statement for 2 of clubs
      System.out.println("2 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card2 == 16){ //start of if statement for 3 of clubs
      System.out.println("3 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card2 == 17){ //start of if statement for 4 of clubs
      System.out.println("4 of clubs"); //prints out the card selected
    } //end of if statement for 4 of clubs
    else if (card2 == 18){ //start of if statement for 5 of clubs
      System.out.println("5 of clubs"); //prints out the card selected
    } //end of if statement for 5 of clubs
    else if (card2 == 19){ //start of if statement for 6 of clubs
      System.out.println("6 of clubs"); //prints out the card selected
    } //end of if statement for 6 of clubs
    else if (card2 == 20){ //start of if statement for 7 of clubs
      System.out.println("7 of clubs"); //prints out the card selected
    } //end of if statement for 7 of clubs
    else if (card2 == 21){ //start of if statement for 8 of clubs
      System.out.println("8 of clubs"); //prints out the card selected
    } //end of if statement for 8 of clubs
    else if (card2 == 22){ //start of if statement for 9 of clubs
      System.out.println("9 of clubs"); //prints out the card selected
    } //end of if statement for 9 of clubs
    else if (card2 == 23){ //start of if statement for 10 of clubs
      System.out.println("10 of clubs"); //prints out the card selected
    } //end of if statement for 10 of clubs
    else if (card2 == 24){ //start of if statement for jack of clubs
      System.out.println("Jack of clubs"); //prints out the card selected
    } //end of if statement for jack of clubs
    else if (card2 == 25){ //start of if statement for queen of clubs
      System.out.println("Queen of clubs"); //prints out the card selected
    } //end of if statement for queen of clubs
    else if (card2 == 26){ //start of if statement for king of clubs
      System.out.println("King of clubs"); //prints out the card selected
    } //end of if statement for king of clubs
    else if (card2 == 27){ //start of if statement for ace of hearts
      System.out.println("Ace of hearts"); //prints out the card selected
    } //end of if statement for ace of hearts
    else if (card2 == 28){ //start of if statement for 2 of hearts
      System.out.println("2 of hearts"); //prints out the card selected
    } //end of if statement for 2 of hearts
    else if (card2 == 29){ //start of if statement for 3 of hearts
      System.out.println("3 of hearts"); //prints out the card selected
    } //end of if statement for 3 of hearts
    else if (card2 == 30){ //start of if statement for 4 of hearts
      System.out.println("4 of hearts"); //prints out the card selected
    } //end of if statement for 4 of hearts
    else if (card2 == 31){ //start of if statement for 5 of hearts
      System.out.println("5 of hearts"); //prints out the card selected
    } //end of if statement for 5 of hearts
    else if (card2 == 32){ //start of if statement for 6 of hearts
      System.out.println("6 of hearts"); //prints out the card selected
    } //end of if statement for 6 of hearts
    else if (card2 == 33){ //start of if statement for 7 of hearts
      System.out.println("7 of hearts"); //prints out the card selected
    } //end of if statement for 7 of hearts
    else if (card2 == 34){ //start of if statement for 8 of hearts
      System.out.println("8 of hearts"); //prints out the card selected
    } //end of if statement for 8 of hearts
    else if (card2 == 35){ //start of if statement for 9 of hearts
      System.out.println("9 of hearts"); //prints out the card selected
    } //end of if statement for 9 of hearts
    else if (card2 == 36){ //start of if statement for 10 of hearts
      System.out.println("10 of hearts"); //prints out the card selected
    } //end of if statement for 10 of hearts
    else if (card2 == 37){ //start of if statement for jack of hearts
      System.out.println("Jack of hearts"); //prints out the card selected
    } //end of if statement for jack of hearts
    else if (card2 == 38){ //start of if statement for queen of hearts
      System.out.println("Queen of hearts"); //prints out the card selected
    } //end of if statement for queen of hearts
    else if (card2 == 39){ //start of if statement for king of hearts
      System.out.println("King of hearts"); //prints out the card selected
    } //end of if statement for king of hearts
    else if (card2 == 40){ //start of if statement for ace of spades
      System.out.println("Ace of spades"); //prints out the card selected
    } //end of if statement for ace of spades
    else if (card2 == 41){ //start of if statement for 2 of spades
      System.out.println("2 of spades"); //prints out the card selected
    } //end of if statement for 2 of spades
    else if (card2 == 42){ //start of if statement for 3 of spades
      System.out.println("3 of spades"); //prints out the card selected
    } //end of if statement for 3 of spades
    else if (card2 == 43){ //start of if statement for 4 of spades
      System.out.println("4 of spades"); //prints out the card selected
    } //end of if statement for 4 of spades
    else if (card2 == 44){ //start of if statement for 5 of spades
      System.out.println("5 of spades"); //prints out the card selected
    } //end of if statement for ace 5 spades
    else if (card2 == 45){ //start of if statement for 6 of spades
      System.out.println("6 of spades"); //prints out the card selected
    } //end of if statement for 6 of spades
    else if (card2 == 46){ //start of if statement for 7 of spades
      System.out.println("7 of spades"); //prints out the card selected
    } //end of if statement for 7 of spades
    else if (card2 == 47){ //start of if statement for 8 of spades
      System.out.println("8 of spades"); //prints out the card selected
    } //end of if statement for 8 of spades
    else if (card2 == 48){ //start of if statement for 9 of spades
      System.out.println("9 of spades"); //prints out the card selected
    } //end of if statement for 9 of spades
    else if (card2 == 49){ //start of if statement for 10 of spades
      System.out.println("10 of spades"); //prints out the card selected
    } //end of if statement for 10 of spades
    else if (card2 == 50){ //start of if statement for jack of spades
      System.out.println("Jack of spades"); //prints out the card selected
    } //end of if statement for jack of spades
    else if (card2 == 51){ //start of if statement for queen of spades
      System.out.println("Queen of spades"); //prints out the card selected
    } //end of if statement for queen of spades
    else if (card2 == 52){ //start of if statement for king of spades
      System.out.println("King of spades"); //prints out the card selected
    } //end of if statement for king of spades
    //int card3 = myScanner.nextInt();
    int card3 = (int)(Math.random() * 52 + 1); //sets the variable to select a random number from 1 to 52
    if (card3 == 1){ //start of if statement for ace of diamonds
      System.out.println("Ace of diamonds"); //prints out the card selected
    } //end of if statement for ace of diamonds
    else if (card3 == 2){ //start of if statement for 2 of diamonds
      System.out.println("2 of diamonds"); //prints out the card selected
    } //end of if statement for 2 of diamonds
    else if (card3 == 3){ //start of if statement for 3 of diamonds
      System.out.println("3 of diamonds"); //prints out the card selected
    } //end of if statement for 3 of diamonds
    else if (card3 == 4){ //start of if statement for 4 of diamonds
      System.out.println("4 of diamonds"); //prints out the card selected
    } //end of if statement for 4 of diamonds
    else if (card3 == 5){ //start of if statement for 5 of diamonds
      System.out.println("5 of diamonds"); //prints out the card selected
    } //end of if statement for 5 of diamonds
    else if (card3 == 6){ //start of if statement for 6 of diamonds
      System.out.println("6 of diamonds"); //prints out the card selected
    } //end of if statement for 6 of diamonds
    else if (card3 == 7){ //start of if statement for 7 of diamonds
      System.out.println("7 of diamonds"); //prints out the card selected
    } //end of if statement for 7 of diamonds
    else if (card3 == 8){ //start of if statement for 8 of diamonds
      System.out.println("8 of diamonds"); //prints out the card selected
    } //end of if statement for 8 of diamonds
    else if (card3 == 9){ //start of if statement for 9 of diamonds
      System.out.println("9 of diamonds"); //prints out the card selected
    } //end of if statement for 9 of diamonds
    else if (card3 == 10){ //start of if statement for 10 of diamonds
      System.out.println("10 of diamonds"); //prints out the card selected
    } //end of if statement for 10 of diamonds
    else if (card3 == 11){ //start of if statement for jack  of diamonds
      System.out.println("Jack of diamonds"); //prints out the card selected
    } //end of if statement for jack of diamonds
    else if (card3 == 12){ //start of if statement for queen of diamonds
      System.out.println("Queen of diamonds"); //prints out the card selected
    } //end of if statement for queen of diamonds
    else if (card3 == 13){ //start of if statement for king of diamonds
      System.out.println("King of diamonds"); //prints out the card selected
    } //end of if statement for king of diamonds
    else if (card3 == 14){ //start of if statement for ace of clubs
      System.out.println("Ace of clubs"); //prints out the card selected
    } //end of if statement for ace of clubs
    else if (card3 == 15){ //start of if statement for 2 of clubs
      System.out.println("2 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card3 == 16){ //start of if statement for 3 of clubs
      System.out.println("3 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card3 == 17){ //start of if statement for 4 of clubs
      System.out.println("4 of clubs"); //prints out the card selected
    } //end of if statement for 4 of clubs
    else if (card3 == 18){ //start of if statement for 5 of clubs
      System.out.println("5 of clubs"); //prints out the card selected
    } //end of if statement for 5 of clubs
    else if (card3 == 19){ //start of if statement for 6 of clubs
      System.out.println("6 of clubs"); //prints out the card selected
    } //end of if statement for 6 of clubs
    else if (card3 == 20){ //start of if statement for 7 of clubs
      System.out.println("7 of clubs"); //prints out the card selected
    } //end of if statement for 7 of clubs
    else if (card3 == 21){ //start of if statement for 8 of clubs
      System.out.println("8 of clubs"); //prints out the card selected
    } //end of if statement for 8 of clubs
    else if (card3 == 22){ //start of if statement for 9 of clubs
      System.out.println("9 of clubs"); //prints out the card selected
    } //end of if statement for 9 of clubs
    else if (card3 == 23){ //start of if statement for 10 of clubs
      System.out.println("10 of clubs"); //prints out the card selected
    } //end of if statement for 10 of clubs
    else if (card3 == 24){ //start of if statement for jack of clubs
      System.out.println("Jack of clubs"); //prints out the card selected
    } //end of if statement for jack of clubs
    else if (card3 == 25){ //start of if statement for queen of clubs
      System.out.println("Queen of clubs"); //prints out the card selected
    } //end of if statement for queen of clubs
    else if (card3 == 26){ //start of if statement for king of clubs
      System.out.println("King of clubs"); //prints out the card selected
    } //end of if statement for king of clubs
    else if (card3 == 27){ //start of if statement for ace of hearts
      System.out.println("Ace of hearts"); //prints out the card selected
    } //end of if statement for ace of hearts
    else if (card3 == 28){ //start of if statement for 2 of hearts
      System.out.println("2 of hearts"); //prints out the card selected
    } //end of if statement for 2 of hearts
    else if (card3 == 29){ //start of if statement for 3 of hearts
      System.out.println("3 of hearts"); //prints out the card selected
    } //end of if statement for 3 of hearts
    else if (card3 == 30){ //start of if statement for 4 of hearts
      System.out.println("4 of hearts"); //prints out the card selected
    } //end of if statement for 4 of hearts
    else if (card3 == 31){ //start of if statement for 5 of hearts
      System.out.println("5 of hearts"); //prints out the card selected
    } //end of if statement for 5 of hearts
    else if (card3 == 32){ //start of if statement for 6 of hearts
      System.out.println("6 of hearts"); //prints out the card selected
    } //end of if statement for 6 of hearts
    else if (card3 == 33){ //start of if statement for 7 of hearts
      System.out.println("7 of hearts"); //prints out the card selected
    } //end of if statement for 7 of hearts
    else if (card3 == 34){ //start of if statement for 8 of hearts
      System.out.println("8 of hearts"); //prints out the card selected
    } //end of if statement for 8 of hearts
    else if (card3 == 35){ //start of if statement for 9 of hearts
      System.out.println("9 of hearts"); //prints out the card selected
    } //end of if statement for 9 of hearts
    else if (card3 == 36){ //start of if statement for 10 of hearts
      System.out.println("10 of hearts"); //prints out the card selected
    } //end of if statement for 10 of hearts
    else if (card3 == 37){ //start of if statement for jack of hearts
      System.out.println("Jack of hearts"); //prints out the card selected
    } //end of if statement for jack of hearts
    else if (card3 == 38){ //start of if statement for queen of hearts
      System.out.println("Queen of hearts"); //prints out the card selected
    } //end of if statement for queen of hearts
    else if (card3 == 39){ //start of if statement for king of hearts
      System.out.println("King of hearts"); //prints out the card selected
    } //end of if statement for king of hearts
    else if (card3 == 40){ //start of if statement for ace of spades
      System.out.println("Ace of spades"); //prints out the card selected
    } //end of if statement for ace of spades
    else if (card3 == 41){ //start of if statement for 2 of spades
      System.out.println("2 of spades"); //prints out the card selected
    } //end of if statement for 2 of spades
    else if (card3 == 42){ //start of if statement for 3 of spades
      System.out.println("3 of spades"); //prints out the card selected
    } //end of if statement for 3 of spades
    else if (card3 == 43){ //start of if statement for 4 of spades
      System.out.println("4 of spades"); //prints out the card selected
    } //end of if statement for 4 of spades
    else if (card3 == 44){ //start of if statement for 5 of spades
      System.out.println("5 of spades"); //prints out the card selected
    } //end of if statement for ace 5 spades
    else if (card3 == 45){ //start of if statement for 6 of spades
      System.out.println("6 of spades"); //prints out the card selected
    } //end of if statement for 6 of spades
    else if (card3 == 46){ //start of if statement for 7 of spades
      System.out.println("7 of spades"); //prints out the card selected
    } //end of if statement for 7 of spades
    else if (card3 == 47){ //start of if statement for 8 of spades
      System.out.println("8 of spades"); //prints out the card selected
    } //end of if statement for 8 of spades
    else if (card3 == 48){ //start of if statement for 9 of spades
      System.out.println("9 of spades"); //prints out the card selected
    } //end of if statement for 9 of spades
    else if (card3 == 49){ //start of if statement for 10 of spades
      System.out.println("10 of spades"); //prints out the card selected
    } //end of if statement for 10 of spades
    else if (card3 == 50){ //start of if statement for jack of spades
      System.out.println("Jack of spades"); //prints out the card selected
    } //end of if statement for jack of spades
    else if (card3 == 51){ //start of if statement for queen of spades
      System.out.println("Queen of spades"); //prints out the card selected
    } //end of if statement for queen of spades
    else if (card3 == 52){ //start of if statement for king of spades
      System.out.println("King of spades"); //prints out the card selected
    } //end of if statement for king of spades
    //int card4 = myScanner.nextInt();
    int card4 = (int)(Math.random() * 52 + 1); //sets the variable to select a random number from 1 to 52
    if (card4 == 1){ //start of if statement for ace of diamonds
      System.out.println("Ace of diamonds"); //prints out the card selected
    } //end of if statement for ace of diamonds
    else if (card4 == 2){ //start of if statement for 2 of diamonds
      System.out.println("2 of diamonds"); //prints out the card selected
    } //end of if statement for 2 of diamonds
    else if (card4 == 3){ //start of if statement for 3 of diamonds
      System.out.println("3 of diamonds"); //prints out the card selected
    } //end of if statement for 3 of diamonds
    else if (card4 == 4){ //start of if statement for 4 of diamonds
      System.out.println("4 of diamonds"); //prints out the card selected
    } //end of if statement for 4 of diamonds
    else if (card4 == 5){ //start of if statement for 5 of diamonds
      System.out.println("5 of diamonds"); //prints out the card selected
    } //end of if statement for 5 of diamonds
    else if (card4 == 6){ //start of if statement for 6 of diamonds
      System.out.println("6 of diamonds"); //prints out the card selected
    } //end of if statement for 6 of diamonds
    else if (card4 == 7){ //start of if statement for 7 of diamonds
      System.out.println("7 of diamonds"); //prints out the card selected
    } //end of if statement for 7 of diamonds
    else if (card4 == 8){ //start of if statement for 8 of diamonds
      System.out.println("8 of diamonds"); //prints out the card selected
    } //end of if statement for 8 of diamonds
    else if (card4 == 9){ //start of if statement for 9 of diamonds
      System.out.println("9 of diamonds"); //prints out the card selected
    } //end of if statement for 9 of diamonds
    else if (card4 == 10){ //start of if statement for 10 of diamonds
      System.out.println("10 of diamonds"); //prints out the card selected
    } //end of if statement for 10 of diamonds
    else if (card4 == 11){ //start of if statement for jack  of diamonds
      System.out.println("Jack of diamonds"); //prints out the card selected
    } //end of if statement for jack of diamonds
    else if (card4 == 12){ //start of if statement for queen of diamonds
      System.out.println("Queen of diamonds"); //prints out the card selected
    } //end of if statement for queen of diamonds
    else if (card4 == 13){ //start of if statement for king of diamonds
      System.out.println("King of diamonds"); //prints out the card selected
    } //end of if statement for king of diamonds
    else if (card4 == 14){ //start of if statement for ace of clubs
      System.out.println("Ace of clubs"); //prints out the card selected
    } //end of if statement for ace of clubs
    else if (card4 == 15){ //start of if statement for 2 of clubs
      System.out.println("2 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card4 == 16){ //start of if statement for 3 of clubs
      System.out.println("3 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card4 == 17){ //start of if statement for 4 of clubs
      System.out.println("4 of clubs"); //prints out the card selected
    } //end of if statement for 4 of clubs
    else if (card4 == 18){ //start of if statement for 5 of clubs
      System.out.println("5 of clubs"); //prints out the card selected
    } //end of if statement for 5 of clubs
    else if (card4 == 19){ //start of if statement for 6 of clubs
      System.out.println("6 of clubs"); //prints out the card selected
    } //end of if statement for 6 of clubs
    else if (card4 == 20){ //start of if statement for 7 of clubs
      System.out.println("7 of clubs"); //prints out the card selected
    } //end of if statement for 7 of clubs
    else if (card4 == 21){ //start of if statement for 8 of clubs
      System.out.println("8 of clubs"); //prints out the card selected
    } //end of if statement for 8 of clubs
    else if (card4 == 22){ //start of if statement for 9 of clubs
      System.out.println("9 of clubs"); //prints out the card selected
    } //end of if statement for 9 of clubs
    else if (card4 == 23){ //start of if statement for 10 of clubs
      System.out.println("10 of clubs"); //prints out the card selected
    } //end of if statement for 10 of clubs
    else if (card4 == 24){ //start of if statement for jack of clubs
      System.out.println("Jack of clubs"); //prints out the card selected
    } //end of if statement for jack of clubs
    else if (card4 == 25){ //start of if statement for queen of clubs
      System.out.println("Queen of clubs"); //prints out the card selected
    } //end of if statement for queen of clubs
    else if (card4 == 26){ //start of if statement for king of clubs
      System.out.println("King of clubs"); //prints out the card selected
    } //end of if statement for king of clubs
    else if (card4 == 27){ //start of if statement for ace of hearts
      System.out.println("Ace of hearts"); //prints out the card selected
    } //end of if statement for ace of hearts
    else if (card4 == 28){ //start of if statement for 2 of hearts
      System.out.println("2 of hearts"); //prints out the card selected
    } //end of if statement for 2 of hearts
    else if (card4 == 29){ //start of if statement for 3 of hearts
      System.out.println("3 of hearts"); //prints out the card selected
    } //end of if statement for 3 of hearts
    else if (card4 == 30){ //start of if statement for 4 of hearts
      System.out.println("4 of hearts"); //prints out the card selected
    } //end of if statement for 4 of hearts
    else if (card4 == 31){ //start of if statement for 5 of hearts
      System.out.println("5 of hearts"); //prints out the card selected
    } //end of if statement for 5 of hearts
    else if (card4 == 32){ //start of if statement for 6 of hearts
      System.out.println("6 of hearts"); //prints out the card selected
    } //end of if statement for 6 of hearts
    else if (card4 == 33){ //start of if statement for 7 of hearts
      System.out.println("7 of hearts"); //prints out the card selected
    } //end of if statement for 7 of hearts
    else if (card4 == 34){ //start of if statement for 8 of hearts
      System.out.println("8 of hearts"); //prints out the card selected
    } //end of if statement for 8 of hearts
    else if (card4 == 35){ //start of if statement for 9 of hearts
      System.out.println("9 of hearts"); //prints out the card selected
    } //end of if statement for 9 of hearts
    else if (card4 == 36){ //start of if statement for 10 of hearts
      System.out.println("10 of hearts"); //prints out the card selected
    } //end of if statement for 10 of hearts
    else if (card4 == 37){ //start of if statement for jack of hearts
      System.out.println("Jack of hearts"); //prints out the card selected
    } //end of if statement for jack of hearts
    else if (card4 == 38){ //start of if statement for queen of hearts
      System.out.println("Queen of hearts"); //prints out the card selected
    } //end of if statement for queen of hearts
    else if (card4 == 39){ //start of if statement for king of hearts
      System.out.println("King of hearts"); //prints out the card selected
    } //end of if statement for king of hearts
    else if (card4 == 40){ //start of if statement for ace of spades
      System.out.println("Ace of spades"); //prints out the card selected
    } //end of if statement for ace of spades
    else if (card4 == 41){ //start of if statement for 2 of spades
      System.out.println("2 of spades"); //prints out the card selected
    } //end of if statement for 2 of spades
    else if (card4 == 42){ //start of if statement for 3 of spades
      System.out.println("3 of spades"); //prints out the card selected
    } //end of if statement for 3 of spades
    else if (card4 == 43){ //start of if statement for 4 of spades
      System.out.println("4 of spades"); //prints out the card selected
    } //end of if statement for 4 of spades
    else if (card4 == 44){ //start of if statement for 5 of spades
      System.out.println("5 of spades"); //prints out the card selected
    } //end of if statement for ace 5 spades
    else if (card4 == 45){ //start of if statement for 6 of spades
      System.out.println("6 of spades"); //prints out the card selected
    } //end of if statement for 6 of spades
    else if (card4 == 46){ //start of if statement for 7 of spades
      System.out.println("7 of spades"); //prints out the card selected
    } //end of if statement for 7 of spades
    else if (card4 == 47){ //start of if statement for 8 of spades
      System.out.println("8 of spades"); //prints out the card selected
    } //end of if statement for 8 of spades
    else if (card4 == 48){ //start of if statement for 9 of spades
      System.out.println("9 of spades"); //prints out the card selected
    } //end of if statement for 9 of spades
    else if (card4 == 49){ //start of if statement for 10 of spades
      System.out.println("10 of spades"); //prints out the card selected
    } //end of if statement for 10 of spades
    else if (card4 == 50){ //start of if statement for jack of spades
      System.out.println("Jack of spades"); //prints out the card selected
    } //end of if statement for jack of spades
    else if (card4 == 51){ //start of if statement for queen of spades
      System.out.println("Queen of spades"); //prints out the card selected
    } //end of if statement for queen of spades
    else if (card4 == 52){ //start of if statement for king of spades
      System.out.println("King of spades"); //prints out the card selected
    } //end of if statement for king of spades
    //int card5 = myScanner.nextInt();
    int card5 = (int)(Math.random() * 52 + 1); //sets the variable to select a random number from 1 to 52
    if (card5 == 1){ //start of if statement for ace of diamonds
      System.out.println("Ace of diamonds"); //prints out the card selected
    } //end of if statement for ace of diamonds
    else if (card5 == 2){ //start of if statement for 2 of diamonds
      System.out.println("2 of diamonds"); //prints out the card selected
    } //end of if statement for 2 of diamonds
    else if (card5 == 3){ //start of if statement for 3 of diamonds
      System.out.println("3 of diamonds"); //prints out the card selected
    } //end of if statement for 3 of diamonds
    else if (card5 == 4){ //start of if statement for 4 of diamonds
      System.out.println("4 of diamonds"); //prints out the card selected
    } //end of if statement for 4 of diamonds
    else if (card5 == 5){ //start of if statement for 5 of diamonds
      System.out.println("5 of diamonds"); //prints out the card selected
    } //end of if statement for 5 of diamonds
    else if (card5 == 6){ //start of if statement for 6 of diamonds
      System.out.println("6 of diamonds"); //prints out the card selected
    } //end of if statement for 6 of diamonds
    else if (card5 == 7){ //start of if statement for 7 of diamonds
      System.out.println("7 of diamonds"); //prints out the card selected
    } //end of if statement for 7 of diamonds
    else if (card5 == 8){ //start of if statement for 8 of diamonds
      System.out.println("8 of diamonds"); //prints out the card selected
    } //end of if statement for 8 of diamonds
    else if (card5 == 9){ //start of if statement for 9 of diamonds
      System.out.println("9 of diamonds"); //prints out the card selected
    } //end of if statement for 9 of diamonds
    else if (card5 == 10){ //start of if statement for 10 of diamonds
      System.out.println("10 of diamonds"); //prints out the card selected
    } //end of if statement for 10 of diamonds
    else if (card5 == 11){ //start of if statement for jack  of diamonds
      System.out.println("Jack of diamonds"); //prints out the card selected
    } //end of if statement for jack of diamonds
    else if (card5 == 12){ //start of if statement for queen of diamonds
      System.out.println("Queen of diamonds"); //prints out the card selected
    } //end of if statement for queen of diamonds
    else if (card5 == 13){ //start of if statement for king of diamonds
      System.out.println("King of diamonds"); //prints out the card selected
    } //end of if statement for king of diamonds
    else if (card5 == 14){ //start of if statement for ace of clubs
      System.out.println("Ace of clubs"); //prints out the card selected
    } //end of if statement for ace of clubs
    else if (card5 == 15){ //start of if statement for 2 of clubs
      System.out.println("2 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card5 == 16){ //start of if statement for 3 of clubs
      System.out.println("3 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (card5 == 17){ //start of if statement for 4 of clubs
      System.out.println("4 of clubs"); //prints out the card selected
    } //end of if statement for 4 of clubs
    else if (card5 == 18){ //start of if statement for 5 of clubs
      System.out.println("5 of clubs"); //prints out the card selected
    } //end of if statement for 5 of clubs
    else if (card5 == 19){ //start of if statement for 6 of clubs
      System.out.println("6 of clubs"); //prints out the card selected
    } //end of if statement for 6 of clubs
    else if (card5 == 20){ //start of if statement for 7 of clubs
      System.out.println("7 of clubs"); //prints out the card selected
    } //end of if statement for 7 of clubs
    else if (card5 == 21){ //start of if statement for 8 of clubs
      System.out.println("8 of clubs"); //prints out the card selected
    } //end of if statement for 8 of clubs
    else if (card5 == 22){ //start of if statement for 9 of clubs
      System.out.println("9 of clubs"); //prints out the card selected
    } //end of if statement for 9 of clubs
    else if (card5 == 23){ //start of if statement for 10 of clubs
      System.out.println("10 of clubs"); //prints out the card selected
    } //end of if statement for 10 of clubs
    else if (card5 == 24){ //start of if statement for jack of clubs
      System.out.println("Jack of clubs"); //prints out the card selected
    } //end of if statement for jack of clubs
    else if (card5 == 25){ //start of if statement for queen of clubs
      System.out.println("Queen of clubs"); //prints out the card selected
    } //end of if statement for queen of clubs
    else if (card5 == 26){ //start of if statement for king of clubs
      System.out.println("King of clubs"); //prints out the card selected
    } //end of if statement for king of clubs
    else if (card5 == 27){ //start of if statement for ace of hearts
      System.out.println("Ace of hearts"); //prints out the card selected
    } //end of if statement for ace of hearts
    else if (card5 == 28){ //start of if statement for 2 of hearts
      System.out.println("2 of hearts"); //prints out the card selected
    } //end of if statement for 2 of hearts
    else if (card5 == 29){ //start of if statement for 3 of hearts
      System.out.println("3 of hearts"); //prints out the card selected
    } //end of if statement for 3 of hearts
    else if (card5 == 30){ //start of if statement for 4 of hearts
      System.out.println("4 of hearts"); //prints out the card selected
    } //end of if statement for 4 of hearts
    else if (card5 == 31){ //start of if statement for 5 of hearts
      System.out.println("5 of hearts"); //prints out the card selected
    } //end of if statement for 5 of hearts
    else if (card5 == 32){ //start of if statement for 6 of hearts
      System.out.println("6 of hearts"); //prints out the card selected
    } //end of if statement for 6 of hearts
    else if (card5 == 33){ //start of if statement for 7 of hearts
      System.out.println("7 of hearts"); //prints out the card selected
    } //end of if statement for 7 of hearts
    else if (card5 == 34){ //start of if statement for 8 of hearts
      System.out.println("8 of hearts"); //prints out the card selected
    } //end of if statement for 8 of hearts
    else if (card5 == 35){ //start of if statement for 9 of hearts
      System.out.println("9 of hearts"); //prints out the card selected
    } //end of if statement for 9 of hearts
    else if (card5 == 36){ //start of if statement for 10 of hearts
      System.out.println("10 of hearts"); //prints out the card selected
    } //end of if statement for 10 of hearts
    else if (card5 == 37){ //start of if statement for jack of hearts
      System.out.println("Jack of hearts"); //prints out the card selected
    } //end of if statement for jack of hearts
    else if (card5 == 38){ //start of if statement for queen of hearts
      System.out.println("Queen of hearts"); //prints out the card selected
    } //end of if statement for queen of hearts
    else if (card5 == 39){ //start of if statement for king of hearts
      System.out.println("King of hearts"); //prints out the card selected
    } //end of if statement for king of hearts
    else if (card5 == 40){ //start of if statement for ace of spades
      System.out.println("Ace of spades"); //prints out the card selected
    } //end of if statement for ace of spades
    else if (card5 == 41){ //start of if statement for 2 of spades
      System.out.println("2 of spades"); //prints out the card selected
    } //end of if statement for 2 of spades
    else if (card5 == 42){ //start of if statement for 3 of spades
      System.out.println("3 of spades"); //prints out the card selected
    } //end of if statement for 3 of spades
    else if (card5 == 43){ //start of if statement for 4 of spades
      System.out.println("4 of spades"); //prints out the card selected
    } //end of if statement for 4 of spades
    else if (card5 == 44){ //start of if statement for 5 of spades
      System.out.println("5 of spades"); //prints out the card selected
    } //end of if statement for ace 5 spades
    else if (card5 == 45){ //start of if statement for 6 of spades
      System.out.println("6 of spades"); //prints out the card selected
    } //end of if statement for 6 of spades
    else if (card5 == 46){ //start of if statement for 7 of spades
      System.out.println("7 of spades"); //prints out the card selected
    } //end of if statement for 7 of spades
    else if (card5 == 47){ //start of if statement for 8 of spades
      System.out.println("8 of spades"); //prints out the card selected
    } //end of if statement for 8 of spades
    else if (card5 == 48){ //start of if statement for 9 of spades
      System.out.println("9 of spades"); //prints out the card selected
    } //end of if statement for 9 of spades
    else if (card5 == 49){ //start of if statement for 10 of spades
      System.out.println("10 of spades"); //prints out the card selected
    } //end of if statement for 10 of spades
    else if (card5 == 50){ //start of if statement for jack of spades
      System.out.println("Jack of spades"); //prints out the card selected
    } //end of if statement for jack of spades
    else if (card5 == 51){ //start of if statement for queen of spades
      System.out.println("Queen of spades"); //prints out the card selected
    } //end of if statement for queen of spades
    else if (card5 == 52){ //start of if statement for king of spades
      System.out.println("King of spades"); //prints out the card selected
    } //end of if statement for king of spades
    if ((card1 == 1) || (card1 == 14) || (card1 == 27) || (card1 == 40)){ //converts values of aces from above to one value
      card1 = 1;
    }
    
    if ((card1 == 2) || (card1 == 15) || (card1 == 28) || (card1 == 41)){ //converts values of 2's from above to one value
      card1 = 2;
    }
    
    if ((card1 == 3) || (card1 == 16) || (card1 == 29) || (card1 == 42)){ //converts values of 3's from above to one value
      card1 = 3;
    }
    
    if ((card1 == 4) || (card1 == 17) || (card1 == 30) || (card1 == 43)){ //converts values of 4's from above to one value
      card1 = 4;
    }
    
    if ((card1 == 5) || (card1 == 18) || (card1 == 31) || (card1 == 44)){ //converts values of 5's from above to one value
      card1 = 5;
    }
    
    if ((card1 == 6) || (card1 == 19) || (card1 == 32) || (card1 == 45)){ //converts values of 6's from above to one value
      card1 = 6;
    }
    
    if ((card1 == 7) || (card1 == 20) || (card1 == 33) || (card1 == 46)){ //converts values of 7's from above to one value
      card1 = 7;
    }
    
    if ((card1 == 8) || (card1 == 21) || (card1 == 34) || (card1 == 47)){ //converts values of 8's from above to one value
      card1 = 8;
    }
    
    if ((card1 == 9) || (card1 == 22) || (card1 == 35) || (card1 == 48)){ //converts values of 9's from above to one value
      card1 = 9;
    }
    
    if ((card1 == 10) || (card1 == 23) || (card1 == 36) || (card1 == 49)){ //converts values of 10's from above to one value
      card1 = 10;
    }
    
    if ((card1 == 11) || (card1 == 24) || (card1 == 37) || (card1 == 50)){ //converts values of 11's from above to one value
      card1 = 11;
    }
    
    if ((card1 == 12) || (card1 == 25) || (card1 == 38) || (card1 == 51)){ //converts values of 12's from above to one value
      card1 = 12;
    }
    
    if ((card1 == 13) || (card1 == 16) || (card1 == 39) || (card1 == 52)){ //converts values of 13's from above to one value
      card1 = 13;
    }
    
    if ((card2 == 1) || (card2 == 14) || (card2 == 27) || (card2 == 40)){ //up until line 1060, the code continues to convert values of the same cards to one common number for cards 2, 3, 4, and 5
      card2 = 1;
    }
    
    if ((card2 == 2) || (card2 == 15) || (card2 == 28) || (card2 == 41)){
      card2 = 2;
    }
    
    if ((card2 == 3) || (card2 == 16) || (card2 == 29) || (card2 == 42)){
      card2 = 3;
    }
    
    if ((card2 == 4) || (card2 == 17) || (card2 == 30) || (card2 == 43)){
      card2 = 4;
    }
    
    if ((card2 == 5) || (card2 == 18) || (card2 == 31) || (card2 == 44)){
      card2 = 5;
    }
    
    if ((card2 == 6) || (card2 == 19) || (card2 == 32) || (card2 == 45)){
      card1 = 6;
    }
    
    if ((card2 == 7) || (card2 == 20) || (card2 == 33) || (card2 == 46)){
      card2 = 7;
    }
    
    if ((card2 == 8) || (card2 == 21) || (card2 == 34) || (card2 == 47)){
      card1 = 8;
    }
    
    if ((card2 == 9) || (card2 == 22) || (card2 == 35) || (card2 == 48)){
      card2 = 9;
    }
    
    if ((card2 == 10) || (card2 == 23) || (card2 == 36) || (card2 == 49)){
      card2 = 10;
    }
    
    if ((card2 == 11) || (card2 == 24) || (card2 == 37) || (card2 == 50)){
      card2 = 11;
    }
    
    if ((card2 == 12) || (card2 == 25) || (card2 == 38) || (card2 == 51)){
      card2 = 12;
    }
    
    if ((card2 == 13) || (card2 == 16) || (card2 == 39) || (card2 == 52)){
      card2 = 13;
    }
    
    if ((card3 == 1) || (card3 == 14) || (card3 == 27) || (card3 == 40)){
      card3 = 1;
    }
    
    if ((card3 == 2) || (card3 == 15) || (card3 == 28) || (card3 == 41)){
      card3 = 2;
    }
    
    if ((card3 == 3) || (card3 == 16) || (card3 == 29) || (card3 == 42)){
      card3 = 3;
    }
    
    if ((card3 == 4) || (card3 == 17) || (card3 == 30) || (card3 == 43)){
      card3 = 4;
    }
    
    if ((card3 == 5) || (card3 == 18) || (card3 == 31) || (card3 == 44)){
      card3 = 5;
    }
    
    if ((card3 == 6) || (card3 == 19) || (card3 == 32) || (card3 == 45)){
      card3 = 6;
    }
    
    if ((card3 == 7) || (card3 == 20) || (card3 == 33) || (card3 == 46)){
      card3 = 7;
    }
    
    if ((card3 == 8) || (card3 == 21) || (card3 == 34) || (card3 == 47)){
      card3 = 8;
    }
    
    if ((card3 == 9) || (card3 == 22) || (card3 == 35) || (card3 == 48)){
      card3 = 9;
    }
    
    if ((card3 == 10) || (card3 == 23) || (card3 == 36) || (card3 == 49)){
      card3 = 10;
    }
    
    if ((card3 == 11) || (card3 == 24) || (card3 == 37) || (card3 == 50)){
      card3 = 11;
    }
    
    if ((card3 == 12) || (card3 == 25) || (card3 == 38) || (card3 == 51)){
      card3 = 12;
    }
    
    if ((card3 == 13) || (card3 == 16) || (card3 == 39) || (card3 == 52)){
      card3 = 13;
    }
    
    if ((card4 == 1) || (card4 == 14) || (card4 == 27) || (card4 == 40)){
      card4 = 1;
    }
    
    if ((card4 == 2) || (card4 == 15) || (card4 == 28) || (card4 == 41)){
      card4 = 2;
    }
    
    if ((card4 == 3) || (card4 == 16) || (card4 == 29) || (card4 == 42)){
      card4 = 3;
    }
    
    if ((card4 == 4) || (card4 == 17) || (card4 == 30) || (card4 == 43)){
      card4 = 4;
    }
    
    if ((card4 == 5) || (card4 == 18) || (card4 == 31) || (card4 == 44)){
      card4 = 5;
    }
    
    if ((card4 == 6) || (card4 == 19) || (card4 == 32) || (card4 == 45)){
      card4 = 6;
    }
    
    if ((card4 == 7) || (card4 == 20) || (card4 == 33) || (card4 == 46)){
      card4 = 7;
    }
    
    if ((card4 == 8) || (card4 == 21) || (card4 == 34) || (card4 == 47)){
      card4 = 8;
    }
    
    if ((card4 == 9) || (card4 == 22) || (card4 == 35) || (card4 == 48)){
      card4 = 9;
    }
    
    if ((card4 == 10) || (card4 == 23) || (card4 == 36) || (card4 == 49)){
      card4 = 10;
    }
    
    if ((card4 == 11) || (card4 == 24) || (card4 == 37) || (card4 == 50)){
      card4 = 11;
    }
    
    if ((card4 == 12) || (card4 == 25) || (card4 == 38) || (card4 == 51)){
      card4 = 12;
    }
    
    if ((card4 == 13) || (card4 == 16) || (card4 == 39) || (card4 == 52)){
      card4 = 13;
    }
    
    if ((card5 == 1) || (card5 == 14) || (card5 == 27) || (card5 == 40)){
      card5 = 1;
    }
    
    if ((card5 == 2) || (card5 == 15) || (card5 == 28) || (card5 == 41)){
      card5 = 2;
    }
    
    if ((card5 == 3) || (card5 == 16) || (card5 == 29) || (card5 == 42)){
      card5 = 3;
    }
    
    if ((card5 == 4) || (card5 == 17) || (card5 == 30) || (card5 == 43)){
      card5 = 4;
    }
    
    if ((card5 == 5) || (card5 == 18) || (card5 == 31) || (card5 == 44)){
      card5 = 5;
    }
    
    if ((card5 == 6) || (card5 == 19) || (card5 == 32) || (card5 == 45)){
      card5 = 6;
    }
    
    if ((card5 == 7) || (card5 == 20) || (card5 == 33) || (card5 == 46)){
      card5 = 7;
    }
    
    if ((card5 == 8) || (card5 == 21) || (card5 == 34) || (card5 == 47)){
      card5 = 8;
    }
    
    if ((card5 == 9) || (card5 == 22) || (card5 == 35) || (card5 == 48)){
      card5 = 9;
    }
    
    if ((card5 == 10) || (card5 == 23) || (card5 == 36) || (card5 == 49)){
      card5 = 10;
    }
    
    if ((card5 == 11) || (card5 == 24) || (card5 == 37) || (card5 == 50)){
      card5 = 11;
    }
    
    if ((card5 == 12) || (card5 == 25) || (card5 == 38) || (card5 == 51)){
      card5 = 12;
    }
    
    if ((card5 == 13) || (card5 == 16) || (card5 == 39) || (card5 == 52)){
      card5 = 13;
    }
    if ((card1 == card2) && (card2 == card3)){ //if cards 1, 2, and 3 all have the same value, there is a three of a kind
      System.out.println("You have three of a kind");
    }
    else if ((card1 == card2) && (card2 == card4)){ //up untl line 1090, the code will compare cards of three of a kind
      System.out.println("You have three of a kind");
    }
    else if ((card1 == card2) && (card2 == card5)){
      System.out.println("You have three of a kind");
    }
    else if ((card1 == card3) && (card3 == card4)){
      System.out.println("You have three of a kind");
    }
    else if ((card1 == card3) && (card3 == card5)){
      System.out.println("You have three of a kind");
    }
    else if ((card1 == card4) && (card4 == card5)){
      System.out.println("You have three of a kind");
    }
    else if ((card2 == card3) && (card3 == card4)){
      System.out.println("You have three of a kind");
    }
    else if ((card2 == card3) && (card3 == card5)){
      System.out.println("You have three of a kind");
    }
    else if ((card2 == card4) && (card4 == card5)){
      System.out.println("You have three of a kind");
    }
    else if ((card3 == card4) && (card4 == card5)){
      System.out.println("You have three of a kind");
    }
    else if ((card1 == card2) && (card3 == card4)){ //the code will compare cards 1, 2, 3, and 4 to see if there is a two pair
      System.out.println("You have a two pair");
    }
     else if ((card1 == card2) && (card3 == card5)){ //up to line 1135, the code will check for two pairs
      System.out.println("You have a two pair");
    }
     else if ((card1 == card2) && (card4 == card5)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card3) && (card2 == card4)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card3) && (card2 == card5)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card3) && (card4 == card5)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card4) && (card2 == card3)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card4) && (card2 == card5)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card4) && (card3 == card5)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card5) && (card2 == card3)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card5) && (card2 == card4)){
      System.out.println("You have a two pair");
    }
    else if ((card1 == card5) && (card3 == card4)){
      System.out.println("You have a two pair");
    }
    else if ((card2 == card3) && (card4 == card5)){
      System.out.println("You have a two pair");
    }
    else if ((card2 == card4) && (card3 == card5)){
      System.out.println("You have a two pair");
    }
    else if ((card2 == card5) && (card3 == card4)){
      System.out.println("You have a two pair");
    }
    else if (card1 == card2){ //the code compares if cards 1 and 2 are a pair
      System.out.println("You have a pair");
    }
    else if (card1 == card3){ //up to line 1165, the code will check to see if there is a pair
      System.out.println("You have a pair");
    }
    else if (card1 == card4){
      System.out.println("You have a pair");
    }
    else if (card1 == card5){
      System.out.println("You have a pair");
    }
    else if (card2 == card3){
      System.out.println("You have a pair");
    }
    else if (card2 == card4){
      System.out.println("You have a pair");
    }
    else if (card2 == card5){
      System.out.println("You have a pair");
    }
    else if (card3 == card4){
      System.out.println("You have a pair");
    }
    else if (card3 == card5){
      System.out.println("You have a pair");
    }
    else if (card4 == card5){
      System.out.println("You have a pair");
    }
    else { //any other scenario will result in a high card hand
      System.out.println("You have a high card hand");
    }
  }
}