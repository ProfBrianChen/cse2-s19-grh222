//Grant Hershman
//CSE2
//April 12, 2019
//This program generates a random array and prints it. It then asks what program to run next: shorten or insert.
//Shorten prompts the user to input 5 integars, then to ask what number they want removed. The program removes that number.
//Insert asks for user to input two arrays of 5 integars. Then, it prompts the user for an integar of how to split up the array.
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;
public class ArrayGames{//start of class
  public static void main(String[] args){//main method
    Scanner scan = new Scanner(System.in);
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(20);//random number for size of array
    while (randomInt<10){//ensures array is more than 10 numbers
      randomInt = randomGenerator.nextInt(20);
    }
    System.out.println("Number of ints: " + randomInt);
    int[] x = generate(randomInt);//calls generate method
    print(x, randomInt);//calls print method
    System.out.println();
    System.out.print("shorten or insert: ");
    String input = scan.next();
    String yes = "shorten";
    String no = "insert";
    while(!input.equals(yes) && !input.equals(no)){//checks input
      System.out.print("shorten or insert: ");
      input = scan.next();
    }
    if (input.equals(yes)){//runs shorten method if input
      shorten();
    }
    if (input.equals(no)){//runs insert method if input
      insert();
    }
  }
  public static int[] generate(int randomInt){//generates random array
    Random randomGenerator = new Random();
    int[] numArrays;
    numArrays = new int[randomInt];
    for (int i = 0; i <= (randomInt-1); i++){
      int randomNum = randomGenerator.nextInt(99);
      numArrays[i] = randomNum;
    }
    return numArrays;
  }
  public static void print(int[] numArrays, int randomInt){//prints random array
    for (int i = 0; i <= (randomInt-1); i++){
      System.out.print(numArrays[i] + " ");
    }
  }
  public static void insert(){//insert method
    Scanner scan = new Scanner(System.in);
    int[] array1;
    array1 = new int[5];
    int[] array2;
    array2 = new int[5];
    System.out.println("Enter five integars: ");
    for (int i = 0; i<=4; i++){
      array1[i] = scan.nextInt();//input for array of size 5
    }
    System.out.println("Enter five integars: ");
    for (int i = 0; i<=4; i++){
      array2[i] = scan.nextInt();//input for array of size 5
    }
    System.out.print("Array 1: {");
    for (int i = 0; i<=4; i++){//prints first array
      System.out.print(array1[i]);
      if (i<4){
        System.out.print(", ");
      }
    }
    System.out.println("}");
    System.out.print("Array 2: {");
    for (int i = 0; i<=4; i++){//prints second array
      System.out.print(array2[i]);
      if (i<4){
        System.out.print(", ");
      }
    }
    System.out.println("}");
    System.out.print("Enter a number less than 6: ");
    int num = scan.nextInt();
    while (num > 5){
      System.out.print("Enter a number less than 6: ");
      num = scan.nextInt();
    }
    System.out.print("{");
    for (int i = 0; i<=(num-1); i++){
      System.out.print(array1[i] + ", ");
    }
    for (int i = 0; i<=4; i++){
      System.out.print(array2[i] + ", ");
    }
    for (int i = num; i<=4; i++){
      System.out.print(array1[i]);
      if (i<4){
        System.out.print(", ");
      }
    }
    System.out.println("}");
  }
  public static void shorten(){//shorten method
    Scanner scan = new Scanner(System.in);
    int[] array1;
    array1 = new int[5];
    System.out.println("Enter five integars: ");
    for (int i = 0; i<=4; i++){//input five integars
      array1[i] = scan.nextInt();
    }
    System.out.print("Array: {");
    for (int i = 0; i<=4; i++){
      System.out.print(array1[i]);
      if (i<4){
        System.out.print(", ");
      }
    }
    System.out.println("}");
    System.out.print("Enter a number you want removed: ");
    int num = scan.nextInt();
    while ((num != array1[0]) && (num != array1[1]) && (num != array1[2]) && (num != array1[3]) && (num != array1[4])){//checks to see if input number is actually one of the integars
      System.out.print("Enter a number you want removed: ");
      num = scan.nextInt();
    }
    System.out.print("{");
    for (int i = 0; i<=4; i++){//removes the inputted number
      if (array1[i] != num){
      System.out.print(array1[i]);
      if (i<4){
        System.out.print(", ");
      }
      }
    }
    System.out.println("}");
  }
}//end of class
