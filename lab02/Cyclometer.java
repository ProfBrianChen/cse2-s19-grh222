//Grant Hershman
//CSE2
//February 8, 2019
//This program should record and calculate data for two bicycle trips. The program should print this data showing:
//1) number of minutes for each trip.
//2) Number of counts for each trip. 
//3) the distance of each trip.
//4) distance of both trips combined.4
public class Cyclometer { //start of class
  public static void main(String[] args){ //start of main method
  int secsTrip1=480; //seconds of trip 1
  int secsTrip2=3220; //seconds of trip 2
  int countsTrip1=1561; //counts of trip 1
  int countsTrip2=9037; //counts of trip 2
  double wheelDiameter=27.0; //diameter of wheel 
  double PI=3.14159; //declares pi
  double feetPerMile=5280; //conversion factor for feet per miles
  double inchesPerFoot=12; //conversion factor for inches per foot
  double secondsPerMinute=60; //conversion factor for seconds per minute
  double distanceTrip1, distanceTrip2, totalDistance; //declares the distances of trips 1,2, and 3 as a double type
   
  System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had " + countsTrip1 +" counts."); //prints out time of distance 1 in minutes and the number of counts for the first distance
  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) +" minutes and had " + countsTrip2 +" counts."); //prints out time of distance 2 in minutes and the number of counts for the second distance
  //The two lines of code above prints out a sentance stating the conversion of seconds to minutes and the number of counts
  //Trip 1 was 8 minutes and 1561 counts.
  //Trip 2 was 53 minutes and 9037 counts.
    
  distanceTrip1 = countsTrip1 * wheelDiameter * PI; //calculates the total distance 1
  distanceTrip1/=inchesPerFoot * feetPerMile; //converts total distance to miles
  distanceTrip2 = countsTrip2 * wheelDiameter*PI / inchesPerFoot / feetPerMile; //calculates the total distance 2 and converts to miles
  totalDistance = distanceTrip1 + distanceTrip2; //calculates the total distance of both trips
    
  System.out.println("Trip 1 was " + distanceTrip1 + " miles."); //prints distance 1
  System.out.println("Trip 2 was " + distanceTrip2 + " miles."); //prints distance 2
  System.out.println("The total distance was " + totalDistance + " miles."); //prints total distance 
  } //end of main method
} //end of class