import java.util.Scanner;
public class PatternC{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter number of rows: ");
    int inputRows = scan.nextInt();
    int check = 1;

   for( int rows = 1; rows <= inputRows; rows++){
     for (int tabs = 1; tabs <= (inputRows - check); tabs++){
       System.out.print(" ");
     }
      for ( int values = check; values >= 1; values--){
        System.out.print(values);
      }
     check++;
      System.out.println();
  }
}
}