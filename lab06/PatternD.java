import java.util.Scanner;
public class PatternD{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter number of rows: ");
    int inputRows = scan.nextInt();

    for( int rows = 1; rows <= inputRows; rows++){
      for ( int values = inputRows; values >= rows; values--){
        
        System.out.print(values + " ");
      }
      System.out.println();
  }
}
}