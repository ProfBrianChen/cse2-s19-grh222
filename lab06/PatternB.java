import java.util.Scanner;
public class PatternB{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter number of rows: ");
    int inputRows = scan.nextInt();
    int check = inputRows;
    for( int rows = 1; rows <= inputRows; rows++){
      
      for ( int values = 1; values <= check; values++){
        
        System.out.print(values + " ");
        
      }
      check--;
      System.out.println();
      
  }
}
}