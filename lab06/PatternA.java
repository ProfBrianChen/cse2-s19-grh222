import java.util.Scanner;
public class PatternA{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter number of rows: ");
    int inputRows = scan.nextInt();
    String output = "";
    for( int rows = 1; rows <= inputRows; rows++){
      for ( int values = 1; values <= rows; values++){
        
        System.out.print(values + " ");
      }
      System.out.println();
    }
  }
}