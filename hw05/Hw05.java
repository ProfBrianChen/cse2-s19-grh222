//Grant Hershman
//March 4, 2019
//CSE2
//This program takes inputs from the user determines if the inputs are the corrct type. If not, it will
//prompt the user to type in the correct type of data

import java.util.Scanner; //imports scanner function
public class Hw05{ //start of class
  public static void main(String[] args){ //start of main method
    Scanner scan = new Scanner(System.in); //initializes scanner
    System.out.print("Type course number: "); //tells user to input data
    int courseNum; //initializes integar
    while (!scan.hasNextInt()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type positive integar: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    courseNum = scan.nextInt(); //takes input from user
    
    
    System.out.print("Type name of department: "); //tells user to input data
    String department; //initializes integar
    while (!scan.hasNext()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type String: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    department = scan.next(); //takes input from user
    
    
    System.out.print("Type number of times you meet per week: "); //tells user to input data
    int numTimes; //initializes integar
    while (!scan.hasNextInt()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type positive integar: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    numTimes = scan.nextInt(); //takes input from user
    
    
    System.out.print("Type time of day you have class (xx:xx): "); //tells user to input data
    String timeDay; //initializes integar
    while (!scan.hasNext()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type time (xx:xx): "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    timeDay = scan.next(); //takes input from user
    
    
    System.out.print("Type first name of instructor: "); //tells user to input data
    String instructerName; //initializes integar
    while (!scan.hasNext()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type string: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    instructerName = scan.next(); //takes input from user
    
    
    System.out.print("Type last name of instructor: "); //tells user to input data
    String instructerLastName; //initializes integar
    while (!scan.hasNext()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type string: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    instructerLastName = scan.next(); //takes input from user
    
    
    System.out.print("Type the number of students: "); //tells user to input data
    int numStudents; //initializes integar
    while (!scan.hasNextInt()){ //while loop that checks whether input is correct type
      System.out.print("Invalid input. Type positive integar: "); //tells user to input data
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    numStudents = scan.nextInt(); //takes input from user
    
    System.out.println("Course number: " + courseNum); //prints course number
    System.out.println("Name of department: " + department); //prints department name
    System.out.println("Number of times you meet per week: " + numTimes); //prints number of times you meet per week
    System.out.println("Time of class: " + timeDay); //prints time of class
    System.out.println("Name of instructor: " + instructerName + " " + instructerLastName); //prints name of instructor
    System.out.println("Number of students: " + numStudents); //prints number of students
  } //end of main method
} //end of class