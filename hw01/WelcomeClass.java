public class WelcomeClass{
  public static void main(String args[]){
    System.out.println("     -----------\n     | WELCOME |\n     ----------- \n   ^  ^  ^  ^  ^  ^\n  / \\/ \\/ \\/ \\/ \\/ \\ \n <-G--R--H--2--2--2-> \n  \\ /\\ /\\ /\\ /\\ /\\ / \n   v  v  v  v  v  v \nMy name is Grant Hershman. I am a freshman here at Lehigh.\nI am from Bethlehem, and my favorite sports teams are\nthe NY Giants, the LA Lakers, and the Boston Red Sox.");
  }
 }                     

