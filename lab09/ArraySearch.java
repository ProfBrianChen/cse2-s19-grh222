//Grant Hershman
//CSE2
//April 12, 2019
//This program will
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class ArraySearch{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    int[] x = Array1();
    int[] y = Array2();
    System.out.print("linear or binary: ");
    String input = scan.next();
    String linear = "linear";
    String binary = "binary";
    if (input.equals(linear)){
      boolean g = Search();
      if(g == false){
        System.out.println("-1");
      }
    }
    if (input.equals(binary)){//runs insert method if input
      BinarySearch();
    }

  }

  public static int[] Array1(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(25);
    int[] numArrays;
    numArrays = new int[randomInt];
    System.out.println("Array: ");
    for (int i = 0; i <= (randomInt-1); i++){
      int randomNum = randomGenerator.nextInt(25);
      numArrays[i] = randomNum;
      System.out.print(numArrays[i] + " ");
    }
    System.out.println();
    return numArrays;
  }

  public static int[] Array2(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(25);
    int[] numArrays;
    numArrays = new int[randomInt];
    System.out.println("Ascending Array: ");
    for (int i = 0; i <= (randomInt-1); i++){
      int randomNum = randomGenerator.nextInt(25);
      numArrays[i] = randomNum;
    }
    Arrays.sort(numArrays);
    for (int number : numArrays){
      System.out.print(number + " ");
    }
    System.out.println();
    return numArrays;
  }

  public static boolean Search(){
    Scanner scan = new Scanner(System.in);
    System.out.println("Input 10 integars: ");
    int[] numArrays;
    numArrays = new int[10];
    for (int i = 0; i <= 9; i++){
      System.out.print("Number " + (i+1) + ": ");
      int z = scan.nextInt();
      numArrays[i] = z;
    }
    System.out.print("Enter a value to search: ");
    int input = scan.nextInt();
    boolean check = false;
    for (int i = 0; i <= 9; i++){
      if (numArrays[i] == input){
        System.out.println("Found at numArrays: " + (i+1));
        check = true;
        break;
      }
    }
    return check;
  }
  public static void BinarySearch(){
    Scanner scan = new Scanner(System.in);
    System.out.println("Input 10 integars: ");
    int[] numArrays;
    numArrays = new int[10];
    for (int i = 0; i <= 9; i++){
      System.out.print("Number " + (i+1) + ": ");
      int z = scan.nextInt();
      numArrays[i] = z;
    }
    Arrays.sort(numArrays);
    System.out.print("Enter a value to search: ");
    int input = scan.nextInt();
    int mid = (numArrays[0] + numArrays[9])/2;
    while(numArrays[0] <= numArrays[9]){
      if (numArrays[mid] < input){
        numArrays[0] = mid + 1;
      }
      else if (numArrays[mid] == input ){
        System.out.println("Found at numArrays: " + (mid + 1));
        break;
      }
      else{
         numArrays[9] = mid - 1;
      }
      mid = (numArrays[0] + numArrays[9])/2;
   }
   if (numArrays[0] > numArrays[9]){
      System.out.println("-1");
   }
  }
  }
