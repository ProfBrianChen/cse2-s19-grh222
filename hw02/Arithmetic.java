//Grant Hershman
//CSE2
//February 8, 2019
//This program calculates costs of a certain number of items and their total costs with tax
public class Arithmetic { //start of class
  public static void main(String[] args){ //Start of main method
    int numPants = 3; //number of pants as integar
    double pantsPrice = 34.98; //cost per pair of pants as double
    int numShirts = 2; //number of sweatshirts as integar
    double shirtPrice = 24.99; //cost per shirt
    int numBelts = 1; //number of belts as an integar
    double beltCost = 33.99; //cost per belt 
    double paSalesTax = 0.06; //sales tax in PA as double 
    
    double totalCostPants = numPants * pantsPrice * (1 + paSalesTax); //calculates total cost of pants
    totalCostPants = ((int) (totalCostPants * 100)) / 100.0; //rounds cost of pants to two decimal places
    double totalCostShirts = numShirts * shirtPrice * (1 + paSalesTax); //calculates total cost of shirts
    totalCostShirts = ((int) (totalCostShirts * 100)) / 100.0; //rounds cost of shirts to two decimal places
    double totalCostBelt = numBelts * beltCost * (1 + paSalesTax); //calculates total cost of belts
    totalCostBelt = ((int) (totalCostBelt * 100)) / 100.0; //rounds cost of belts to two decimal places
    double pantsTax = numPants * pantsPrice * paSalesTax; //calculates total cost of pants tax
    pantsTax = ((int) (pantsTax * 100)) / 100.0; //rounds cost of pants tax to two decimal places
    double shirtsTax = numShirts * shirtPrice * paSalesTax; //calculates total cost of shirts tax
    shirtsTax = ((int) (shirtsTax * 100)) / 100.0; //rounds cost of shirt tax to two decimal places
    double beltTax = numBelts * beltCost * paSalesTax; //calculates total cost of belts tax
    beltTax = ((int) (beltTax * 100)) / 100.0; //rounds cost of belt tax to two decimal places
    double costNoTax = (numPants * pantsPrice) + (numShirts * shirtPrice) + (numBelts * beltCost); //calculates total cost of purchase without tax
    costNoTax = ((int) (costNoTax * 100)) / 100.0; //rounds total cost of purchase without tax to two decimal places
    double totalTax = pantsTax + shirtsTax + beltTax; //total cost of tax
    totalTax = ((int) (totalTax * 100)) / 100.0; //rounds total cost of tax to two decimal places
    double totalCost = totalCostPants + totalCostShirts + totalCostBelt; //calculates the total cost
    totalCost = ((int) (totalCost * 100)) / 100.0; //rounds total cost to two decimal places
    
    System.out.println("Total cost of Pants = $" + totalCostPants); //prints out total cost of pants in terminal
    System.out.println("Total cost of Shirts = $" + totalCostShirts); //prints out total cost of shirts in terminal
    System.out.println("Total cost of Belt = $" +totalCostBelt); //prints out total cost of belts in terminal
    System.out.println("Pants tax = $" + pantsTax); //prints out total cost of pants' tax in terminal
    System.out.println("Shirts tax = $" + shirtsTax); //prints out total cost of shirts' tax in terminal
    System.out.println("Belt tax = $" + beltTax); //prints out total cost of belts' tax in terminal
    System.out.println("Total cost without tax = $" + costNoTax); //prints out total cost of purchase without tax
    System.out.println("Total cost of tax = $" + totalTax); //prints out total cost of tax
    System.out.println("The total cost of the transaction = $" + totalCost); //prints out the final cost of the transaction
  } //end of main method
} //end of class