//Grant Hershman
//CSE2
//April 5, 2019
//vienvijervbj
import java.util.Arrays;
import java.util.Random;
import java.lang.Math;
public class ArrayLab{
  public static void main (String[] args){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(100);
    while (randomInt<50){
      randomInt = randomGenerator.nextInt(100);
    }
    int[] numArrays;
    numArrays = new int[randomInt];
    System.out.println("Number of values: " + randomInt);
    System.out.println("Array: ");
    for (int i = 0; i <= (randomInt-1); i++){
      int randomNum = randomGenerator.nextInt(99);
      numArrays[i] = randomNum;
      System.out.print(numArrays[i] + " ");
    }
    System.out.println();
    System.out.println("Sorted array: ");
    int y = getRange(numArrays, randomInt);
    double x = getMean(y, randomInt);
    getStdDev(x, numArrays, randomInt);
    System.out.println("Shuffled: ");
    for (int i = 0; i <= (randomInt-1); i++){
      int randomNum = randomGenerator.nextInt(99);
      numArrays[i] = randomNum;
      System.out.print(numArrays[i] + " ");
    }
    System.out.println();
  }
  public static int getRange(int[] numArrays, int randomInt){
    Arrays.sort(numArrays);
    for (int number : numArrays){
      System.out.print(number + " ");
    }
    int min = numArrays[0];
    int max = numArrays[randomInt - 1];
    int range = max - min;
    System.out.println();
    System.out.println("Range: " + range);
    return range;
  }
  public static double getMean(int range, int randomInt){
    double mean = range/2;
    System.out.println("Mean: " + mean);
    return mean;
  }
  public static double getStdDev(double mean, int[] numArrays, int randomInt){
    double sum = 0;
    for (int number : numArrays){
      sum = sum + (number-mean);
    }
    double std = Math.sqrt(sum * 2/(randomInt-1));
    System.out.println("Standard Deviation: " + std);
    return std;
  }
}
