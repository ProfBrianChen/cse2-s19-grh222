import java.util.Random;
public class CardGeneratorV2 {
 
  public static void main(String[] args){
    int randomNum = (int) (Math.random() * 13 + 1);
    int suit = (int) (Math.random() * 4 + 1);
    String suitName = "card";
    String specialCard = "win";
    if (randomNum == 1){
      specialCard = "Ace";
    }
    else if (randomNum == 11){
      specialCard = "Jack";
    }
    else if (randomNum == 12){
      specialCard = "Queen";
    }
    else if (randomNum == 13){
      specialCard = "King";
    }
    if (suit == 1){
      suitName = "hearts";
    }
    else if (suit == 2){
      suitName = "diamonds";
    }
    else if (suit == 3){
      suitName = "spades";
    }
    else {
      suitName = "clubs";
    }
    
    System.out.println(randomNum + " of " + suitName);
  }
 
}