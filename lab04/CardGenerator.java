//Grant Hershman
//CSE2
//February 15, 2019
//This program will use a random number generator to simulate picking a random card from a standard deck of 52 cards
import java.util.Random;
public class CardGenerator{ //start of class
  public static void main(String[] args){ //start of main method
    int randomCard = (int)(Math.random() * 52 + 1); //sets the variable to select a random number from 1 to 52
    if (randomCard == 1){ //start of if statement for ace of diamonds
      System.out.println("Ace of diamonds"); //prints out the card selected
    } //end of if statement for ace of diamonds
    else if (randomCard == 2){ //start of if statement for 2 of diamonds
      System.out.println("2 of diamonds"); //prints out the card selected
    } //end of if statement for 2 of diamonds
    else if (randomCard == 3){ //start of if statement for 3 of diamonds
      System.out.println("3 of diamonds"); //prints out the card selected
    } //end of if statement for 3 of diamonds
    else if (randomCard == 4){ //start of if statement for 4 of diamonds
      System.out.println("4 of diamonds"); //prints out the card selected
    } //end of if statement for 4 of diamonds
    else if (randomCard == 5){ //start of if statement for 5 of diamonds
      System.out.println("5 of diamonds"); //prints out the card selected
    } //end of if statement for 5 of diamonds
    else if (randomCard == 6){ //start of if statement for 6 of diamonds
      System.out.println("6 of diamonds"); //prints out the card selected
    } //end of if statement for 6 of diamonds
    else if (randomCard == 7){ //start of if statement for 7 of diamonds
      System.out.println("7 of diamonds"); //prints out the card selected
    } //end of if statement for 7 of diamonds
    else if (randomCard == 8){ //start of if statement for 8 of diamonds
      System.out.println("8 of diamonds"); //prints out the card selected
    } //end of if statement for 8 of diamonds
    else if (randomCard == 9){ //start of if statement for 9 of diamonds
      System.out.println("9 of diamonds"); //prints out the card selected
    } //end of if statement for 9 of diamonds
    else if (randomCard == 10){ //start of if statement for 10 of diamonds
      System.out.println("10 of diamonds"); //prints out the card selected
    } //end of if statement for 10 of diamonds
    else if (randomCard == 11){ //start of if statement for jack  of diamonds
      System.out.println("Jack of diamonds"); //prints out the card selected
    } //end of if statement for jack of diamonds
    else if (randomCard == 12){ //start of if statement for queen of diamonds
      System.out.println("Queen of diamonds"); //prints out the card selected
    } //end of if statement for queen of diamonds
    else if (randomCard == 13){ //start of if statement for king of diamonds
      System.out.println("King of diamonds"); //prints out the card selected
    } //end of if statement for king of diamonds
    else if (randomCard == 14){ //start of if statement for ace of clubs
      System.out.println("Ace of clubs"); //prints out the card selected
    } //end of if statement for ace of clubs
    else if (randomCard == 15){ //start of if statement for 2 of clubs
      System.out.println("2 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (randomCard == 16){ //start of if statement for 3 of clubs
      System.out.println("3 of clubs"); //prints out the card selected
    } //end of if statement for 3 of clubs
    else if (randomCard == 17){ //start of if statement for 4 of clubs
      System.out.println("4 of clubs"); //prints out the card selected
    } //end of if statement for 4 of clubs
    else if (randomCard == 18){ //start of if statement for 5 of clubs
      System.out.println("5 of clubs"); //prints out the card selected
    } //end of if statement for 5 of clubs
    else if (randomCard == 19){ //start of if statement for 6 of clubs
      System.out.println("6 of clubs"); //prints out the card selected
    } //end of if statement for 6 of clubs
    else if (randomCard == 20){ //start of if statement for 7 of clubs
      System.out.println("7 of clubs"); //prints out the card selected
    } //end of if statement for 7 of clubs
    else if (randomCard == 21){ //start of if statement for 8 of clubs
      System.out.println("8 of clubs"); //prints out the card selected
    } //end of if statement for 8 of clubs
    else if (randomCard == 22){ //start of if statement for 9 of clubs
      System.out.println("9 of clubs"); //prints out the card selected
    } //end of if statement for 9 of clubs
    else if (randomCard == 23){ //start of if statement for 10 of clubs
      System.out.println("10 of clubs"); //prints out the card selected
    } //end of if statement for 10 of clubs
    else if (randomCard == 24){ //start of if statement for jack of clubs
      System.out.println("Jack of clubs"); //prints out the card selected
    } //end of if statement for jack of clubs
    else if (randomCard == 25){ //start of if statement for queen of clubs
      System.out.println("Queen of clubs"); //prints out the card selected
    } //end of if statement for queen of clubs
    else if (randomCard == 26){ //start of if statement for king of clubs
      System.out.println("King of clubs"); //prints out the card selected
    } //end of if statement for king of clubs
    else if (randomCard == 27){ //start of if statement for ace of hearts
      System.out.println("Ace of hearts"); //prints out the card selected
    } //end of if statement for ace of hearts
    else if (randomCard == 28){ //start of if statement for 2 of hearts
      System.out.println("2 of hearts"); //prints out the card selected
    } //end of if statement for 2 of hearts
    else if (randomCard == 29){ //start of if statement for 3 of hearts
      System.out.println("3 of hearts"); //prints out the card selected
    } //end of if statement for 3 of hearts
    else if (randomCard == 30){ //start of if statement for 4 of hearts
      System.out.println("4 of hearts"); //prints out the card selected
    } //end of if statement for 4 of hearts
    else if (randomCard == 31){ //start of if statement for 5 of hearts
      System.out.println("5 of hearts"); //prints out the card selected
    } //end of if statement for 5 of hearts
    else if (randomCard == 32){ //start of if statement for 6 of hearts
      System.out.println("6 of hearts"); //prints out the card selected
    } //end of if statement for 6 of hearts
    else if (randomCard == 33){ //start of if statement for 7 of hearts
      System.out.println("7 of hearts"); //prints out the card selected
    } //end of if statement for 7 of hearts
    else if (randomCard == 34){ //start of if statement for 8 of hearts
      System.out.println("8 of hearts"); //prints out the card selected
    } //end of if statement for 8 of hearts
    else if (randomCard == 35){ //start of if statement for 9 of hearts
      System.out.println("9 of hearts"); //prints out the card selected
    } //end of if statement for 9 of hearts
    else if (randomCard == 36){ //start of if statement for 10 of hearts
      System.out.println("10 of hearts"); //prints out the card selected
    } //end of if statement for 10 of hearts
    else if (randomCard == 37){ //start of if statement for jack of hearts
      System.out.println("Jack of hearts"); //prints out the card selected
    } //end of if statement for jack of hearts
    else if (randomCard == 38){ //start of if statement for queen of hearts
      System.out.println("Queen of hearts"); //prints out the card selected
    } //end of if statement for queen of hearts
    else if (randomCard == 39){ //start of if statement for king of hearts
      System.out.println("King of hearts"); //prints out the card selected
    } //end of if statement for king of hearts
    if (randomCard == 40){ //start of if statement for ace of spades
      System.out.println("Ace of spades"); //prints out the card selected
    } //end of if statement for ace of spades
    else if (randomCard == 41){ //start of if statement for 2 of spades
      System.out.println("2 of spades"); //prints out the card selected
    } //end of if statement for 2 of spades
    else if (randomCard == 42){ //start of if statement for 3 of spades
      System.out.println("3 of spades"); //prints out the card selected
    } //end of if statement for 3 of spades
    else if (randomCard == 43){ //start of if statement for 4 of spades
      System.out.println("4 of spades"); //prints out the card selected
    } //end of if statement for 4 of spades
    else if (randomCard == 44){ //start of if statement for 5 of spades
      System.out.println("5 of spades"); //prints out the card selected
    } //end of if statement for ace 5 spades
    else if (randomCard == 45){ //start of if statement for 6 of spades
      System.out.println("6 of spades"); //prints out the card selected
    } //end of if statement for 6 of spades
    else if (randomCard == 46){ //start of if statement for 7 of spades
      System.out.println("7 of spades"); //prints out the card selected
    } //end of if statement for 7 of spades
    else if (randomCard == 47){ //start of if statement for 8 of spades
      System.out.println("8 of spades"); //prints out the card selected
    } //end of if statement for 8 of spades
    else if (randomCard == 48){ //start of if statement for 9 of spades
      System.out.println("9 of spades"); //prints out the card selected
    } //end of if statement for 9 of spades
    else if (randomCard == 49){ //start of if statement for 10 of spades
      System.out.println("10 of spades"); //prints out the card selected
    } //end of if statement for 10 of spades
    else if (randomCard == 50){ //start of if statement for jack of spades
      System.out.println("Jack of spades"); //prints out the card selected
    } //end of if statement for jack of spades
    else if (randomCard == 51){ //start of if statement for queen of spades
      System.out.println("Queen of spades"); //prints out the card selected
    } //end of if statement for queen of spades
    else if (randomCard == 52){ //start of if statement for king of spades
      System.out.println("King of spades"); //prints out the card selected
    } //end of if statement for king of spades
  } //end of main method
} //end of class