//Grant Hershman
//CSE2
//March 22, 2019
//Random sentence generator

import java.util.Random;
public class RandomSentence{//start of class


public static void main(String[] args){//start of main method
  String x = "a";
  String y = FirstSentence(x);
  String z = SecondSentence(y);
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(5);
  for (int q = 0; q <= randomInt; q++){
    String g = Body();
  }
}
public static String First (){//method for first word
  String output = "";
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(5);
  switch (randomInt){
    case 0:
      output = "The";
      break;
    case 1:
      output = "That";
      break;
    case 2:
      output = "This";
      break;
    case 3:
      output = "His";
      break;
    case 4:
      output = "Her";
      break;
    case 5:
      output = "My";
      break;
}
return output;
  }
  public static String article (){//method for second word
    String output = "";
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(5);
    switch (randomInt){
      case 0:
        output = "the";
        break;
      case 1:
        output = "that";
        break;
      case 2:
        output = "this";
        break;
      case 3:
        output = "his";
        break;
      case 4:
        output = "her";
        break;
      case 5:
        output = "my";
        break;
  }
  return output;
    }
  public static String subject (){//method for third word
    String output = "";
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(5);
    switch (randomInt){
      case 0:
        output = "fox";
        break;
      case 1:
        output = "cat";
        break;
      case 2:
        output = "airplane";
        break;
      case 3:
        output = "baseball";
        break;
      case 4:
        output = "criminal";
        break;
      case 5:
        output = "athlete";
        break;
  }
  return output;
    }

    public static String adjective (){//method for fourth word
      String output = "";
      Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(5);
      switch (randomInt){
        case 0:
          output = "fiercely";
          break;
        case 1:
          output = "humbly";
          break;
        case 2:
          output = "awkwardly";
          break;
        case 3:
          output = "hysterically";
          break;
        case 4:
          output = "nicely";
          break;
        case 5:
          output = "religiously";
          break;
    }
    return output;
      }

      public static String verb (){//method for fifth word
        String output = "";
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(5);
        switch (randomInt){
          case 0:
            output = "ran";
            break;
          case 1:
            output = "punched";
            break;
          case 2:
            output = "threw";
            break;
          case 3:
            output = "spun";
            break;
          case 4:
            output = "bamboozled";
            break;
          case 5:
            output = "critiqued";
            break;
      }
      return output;
        }
        public static String object (){//method for sixth word
          String output = "";
          Random randomGenerator = new Random();
          int randomInt = randomGenerator.nextInt(5);//randomly picks number
          switch (randomInt){//genrates word for number
            case 0:
              output = "whipped cream";
              break;
            case 1:
              output = "tower";
              break;
            case 2:
              output = "sword";
              break;
            case 3:
              output = "moose";
              break;
            case 4:
              output = "chocolate bar";
              break;
            case 5:
              output = "eyeball";
              break;
        }
        return output;
          }
      public static String FirstSentence(String x){//method for thesis statement
        x = subject();
        System.out.print(First() + " ");//print statements assembles words
        System.out.print(x + " ");
        System.out.print(adjective() + " ");
        System.out.print(verb() + " ");
        System.out.print(article() + " ");
        System.out.print(object() + ".");
        System.out.println();
        return x;
      }
      public static String SecondSentence(String y){//method for supporting sentence
        System.out.print(First() + " ");
        System.out.print(y + " ");
        System.out.print(adjective() + " ");
        System.out.print(verb() + " ");
        System.out.print(article() + " ");
        System.out.print(object() + ".");
        System.out.println();
        return y;
      }
      public static String Body(){//method to run body sentences
        String check = "";
        System.out.print(First() + " ");
        System.out.print(subject() + " ");
        System.out.print(adjective() + " ");
        System.out.print(verb() + " ");
        System.out.print(article() + " ");
        System.out.print(object() + ".");
        System.out.println();
        return check;
      }
}
