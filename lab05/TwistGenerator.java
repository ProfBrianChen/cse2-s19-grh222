//Grant Hershman
//March 1, 2019
//CSE2
//This program takes inputs from the user determines if the inputs are the corrct type. If not, it will
//prompt the user to type in the correct type of data
import java.util.Scanner; //imports scanner function
public class TwistGenerator{ //start of class
  public static void main(String[] args){ //start of main method
    Scanner scan = new Scanner(System.in); //initializes scanner
    System.out.println("Please type a positive integar:"); //prompts user to type integar
    int length; //initializes length variable
    int count = 0; //initializes counter
    while (!scan.hasNextInt()){ //checks whether or not input is an integaar 
      System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
      String junkWord = scan.next(); //resests scanner for new input
    } //end of while loop to test for integar input
    length = scan.nextInt(); //input for length
    if (length < 0){ //checks if value is creater than zero
      System.out.println("Invalid input. Type positive integar: "); //error message for wrong type
      while (!scan.hasNextInt()){ //checks for valid integar
      String junkWord = scan.next(); //clears scanner
    } //end of while loop
    length = scan.nextInt(); //input for length if value is less than zero
    } //end of if statement
    while (count < length){ //checks if the count is less then length
      System.out.print("\\ / "); //prints top of twist
    
      count++;  //postdecrement to add up until it is equal to the length
    } //end of while loop
    count = 0; //sets count back to zero
    System.out.println(); //starts a new line of print 
    while (count < length){ //the next 11 lines do the same as lines 25-31
     System.out.print(" x  ");
     count++;  
    }
    count = 0;
    System.out.println();
    while (count < length){
      System.out.print("/ \\ ");
      count++;
    }
    count = 0;
    System.out.println();
  }
}