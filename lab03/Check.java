//This program calculates the cost per person of a check at a restaurant. 
//The program will take inputs for the cost of the check, the percent tip you wish to pay, and the number of people paying.
//Finally, it will split the total coast evenly among the number of people paying.
import java.util.Scanner;
//Documentation of program:
  //The program was finished and checked off during Friday lab period
public class Check { //start of class
  public static void main(String[] args) { //start of main method
    Scanner myScanner = new Scanner (System.in); //allows the import from above to be used to create inputs for the code below
    System.out.print("Enter the original value of the check in the form xx.xx: "); //prompt to input the check value
    double checkCost = myScanner.nextDouble(); //converts input from previous line into a double type
    System.out.print("Enter the percentage tip you wish to pay in the form of a whole number xx: "); //prompt to input the percent tip
    double tipPercent = myScanner.nextDouble(); //converts percent tip into a double so it can be converted into a decimal when divided by 100
    tipPercent /= 100; //Converting percentage into decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //Prompt in terminal for an input
    int numPeople = myScanner.nextInt(); //creates input for number of people as an integar
    double totalCost; //converts totalCost into a double type
    double costPerPerson; //converts costPerPerson to into a double type
    int dollars, //Whole dollar amount of cost
    dimes, pennies; //for storing didgits to the right of the decimal point for the cost 
    totalCost = checkCost * (1 + tipPercent); //multiplies the cost of meal by 1.xx (percnt tip) to create the total cost of the meal
    costPerPerson = totalCost / numPeople; //divides the total cost by the number of people
    dollars = (int)costPerPerson;  //converts cost per person into an integar
    dimes=(int)(costPerPerson * 10) % 10; //get dimes amount, e.g., (int)(6.73 * 10) % 10 -> 67 % 10 -> 7 where the % (mod) operator returns the remainder after the division:   583%100 -> 83, 27%5 -> 2
    pennies=(int)(costPerPerson * 100) % 10; //gets pennies amount 
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
  } //end of main method
} //end of class